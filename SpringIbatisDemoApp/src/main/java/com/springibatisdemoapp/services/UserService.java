package com.springibatisdemoapp.services;

import java.io.IOException;
import java.io.Reader;

import javax.transaction.Transactional;

import org.apache.ibatis.io.Resources;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.UsesSunHttpServer;
import org.springframework.stereotype.Service;

import com.ibatis.sqlmap.client.SqlMapClient;
import com.ibatis.sqlmap.client.SqlMapClientBuilder;
import com.springibatisdemoapp.dao.UserDao;
import com.springibatisdemoapp.dao.UserDaoIbatis;
import com.springibatisdemoapp.entities.User;

/**
 * This class refers to User service which calls the methods from UserDAO
 * @author IMVIZAG
 *
 */
@Service
@Transactional
public class UserService {

	@Autowired
	private UserDao userDao;
	
	public boolean isExisted(String email) {
		 //Create the SQLMapClient
        Reader reader = null;
		try {
			reader = Resources.getResourceAsReader("sqlMapConfig.xml");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        SqlMapClient sqlmapClient = SqlMapClientBuilder.buildSqlMapClient (reader);
        User user = userDao.getUserByEmail(email, sqlmapClient);
        
        if(user != null) {
        	return true;
	}else {
		return false;
	}

	}
	public boolean save(User user) {
		 Reader reader = null;
			try {
				reader = Resources.getResourceAsReader("sqlMapConfig.xml");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	        SqlMapClient sqlmapClient = SqlMapClientBuilder.buildSqlMapClient (reader);
	        User result = userDao.addUser(user,sqlmapClient);
		 if(result!=null)
		 {
			 return true;
		 }
		 else
			 return false;
	}

	public boolean update(User user) {
		return false;
	}

	

	public boolean verifyUser(String email, String password) {
		// TODO Auto-generated method stub
		return false;
	}
}
