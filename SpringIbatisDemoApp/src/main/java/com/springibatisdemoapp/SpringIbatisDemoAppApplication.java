package com.springibatisdemoapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;

@SpringBootApplication
@EnableAutoConfiguration(exclude={DataSourceAutoConfiguration.class})
public class SpringIbatisDemoAppApplication {

	
	public static void main(String[] args) {
		SpringApplication.run(SpringIbatisDemoAppApplication.class, args);
		
		
	}

}
