package com.springibatisdemoapp.dao;

import com.ibatis.sqlmap.client.SqlMapClient;
import com.springibatisdemoapp.entities.User;

public interface UserDao {

	void deleteUserById(Integer id, SqlMapClient sqlmapClient);

	User getUserByEmail(String email, SqlMapClient sqlmapClient);

	User addUser(User user, SqlMapClient sqlmapClient);


}
