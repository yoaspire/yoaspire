package com.springibatisdemoapp.dao;

import org.springframework.stereotype.Repository;

import com.ibatis.sqlmap.client.SqlMapClient;
import com.springibatisdemoapp.entities.User;

@Repository
public class UserDaoIbatis implements UserDao {

	public User addUser(User user, SqlMapClient sqlmapClient) {
        try
        {
        	Integer id = (Integer)sqlmapClient.queryForObject("user.getMaxUserId");
            id = id == null ? 1 : id + 1;
            user.setUserId(id);
            sqlmapClient.insert("user.addUser", user);
            user = getUserByEmail(user.getEmail(), sqlmapClient);
            return user;
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        return null;
    }
 
    @Override
    public User getUserByEmail(String email, SqlMapClient sqlmapClient) {
        try
        {
            User user = (User)sqlmapClient.queryForObject("user.getUserByEmail", email);
            return user;
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        return null;
    }
 
    @Override
    public void deleteUserById(Integer id, SqlMapClient sqlmapClient) {
        try
        {
            sqlmapClient.delete("user.deleteUserById", id);
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

}
