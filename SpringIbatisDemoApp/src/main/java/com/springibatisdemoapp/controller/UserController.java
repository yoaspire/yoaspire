package com.springibatisdemoapp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.springibatisdemoapp.entities.User;
import com.springibatisdemoapp.services.UserService;
import com.springibatisdemoapp.util.ResponseObject;

/**
 * This class refers to user controller which creates api's for user services.
 * @author IMVIZAG
 *
 */
@RestController
@RequestMapping("/api/user")
@CrossOrigin
public class UserController {

	@Autowired
	private UserService userService;
	
	@Autowired
	private ResponseObject responseObject;
	
	
	
	
	@PostMapping("/registerUser")
	public ResponseEntity<ResponseObject> registerUser(@RequestBody User user){
		
		ResponseEntity<ResponseObject> responseEntity = null;
		boolean isExisted = userService.isExisted(user.getEmail());
		boolean isRegistered = false;
		if(!isExisted) {
			isRegistered = userService.save(user);
			if(isRegistered) {
				
			responseObject.setResponse("Registered Successfully");
			responseObject.setStatusCode(1);
			}else {
				responseObject.setResponse("Something wrong");
				responseObject.setStatusCode(-1);
			}
		}else {
			responseObject.setResponse("user with this Email already exists");
			responseObject.setStatusCode(-2);
		}
		responseEntity = new ResponseEntity<ResponseObject>(responseObject, HttpStatus.OK);
		return responseEntity;
		
		
		
	}
	
	@RequestMapping("/loginUser")
	public ResponseEntity<ResponseObject> loginUser(@RequestBody User user){
		
		ResponseEntity<ResponseObject> responseEntity = null;
		String email = user.getEmail();
		String password = user.getPassword();
		boolean validUser = userService.verifyUser(email,password);
		
		if(validUser)
		{
			responseObject.setResponse("Login Successful");
			responseObject.setStatusCode(1);
		}else {
			responseObject.setResponse("Login failed");
			responseObject.setStatusCode(-1);
		}
		responseEntity = new ResponseEntity<ResponseObject>(responseObject, HttpStatus.OK);
		return responseEntity;
		
		}

	@PutMapping("/updateUser")
	public ResponseEntity<ResponseObject> updateUser(@RequestBody User user){
		
		ResponseEntity<ResponseObject> responseEntity = null;
		boolean isExisted = userService.isExisted(user.getEmail());
		boolean isUpdated = false;
		if(isExisted) {
			isUpdated = userService.update(user);
			if(isUpdated) {
			responseObject.setResponse("Updated Successfully");
			responseObject.setStatusCode(1);
			}else {
				responseObject.setResponse("Something wrong");
				responseObject.setStatusCode(-1);
			}
		}else {
			responseObject.setResponse("user with this Email does not exists");
			responseObject.setStatusCode(-2);
		}
		responseEntity = new ResponseEntity<ResponseObject>(responseObject, HttpStatus.OK);
		return responseEntity;
		
	}
	

}
