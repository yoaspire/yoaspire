package com.springibatisdemoapp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.springibatisdemoapp.entities.User;
import com.springibatisdemoapp.services.AdminService;
import com.springibatisdemoapp.services.UserService;
import com.springibatisdemoapp.util.ResponseObject;

/**
 * This class refers to admin controller which creates api's for admin services.
 * @author IMVIZAG
 *
 */
@RestController("/admin")
public class AdminController {
	
	@Autowired
	private AdminService adminService;
	
	@Autowired
	private ResponseObject  responseObject;
	
	@Autowired
	private UserService userService;
	
  	@GetMapping("/displayUsers")
	public ResponseEntity<ResponseObject> displayAllUsers()
	{
  		ResponseEntity<ResponseObject> responseEntity = null;
  		List<User> userList = adminService.displayAllUsers();
  		
  		if(userList.size() != 0) {
  			responseObject.setResponse(userList);
			responseObject.setStatusCode(1);
  		}else {
  			responseObject.setResponse("No User Found");
			responseObject.setStatusCode(-1);
  		}
		responseEntity = new ResponseEntity<ResponseObject>(responseObject, HttpStatus.OK);

		return responseEntity;
	}
  	
  	@DeleteMapping("/deleteUser")
	public ResponseEntity<ResponseObject> deleteUsers(@RequestBody User user)
	{
  		ResponseEntity<ResponseObject> responseEntity = null;
  		String email = user.getEmail();
  		boolean isExisted = userService.isExisted(user.getEmail());
         boolean isDeleted = adminService.deleteUser(email);
         if(isExisted) {
	         if(isDeleted) {
	   			responseObject.setResponse("Deleted successfully");
	 			responseObject.setStatusCode(1);
	   		}else {
	  		
	   			responseObject.setResponse("user deletion failed");
	 			responseObject.setStatusCode(-1);
	   		}
         }else {
        	 responseObject.setResponse("User with this Email does not exists");
	 		 responseObject.setStatusCode(1);
         }
         responseEntity = new ResponseEntity<ResponseObject>(responseObject, HttpStatus.OK);

 		return responseEntity;

	
	}
}
