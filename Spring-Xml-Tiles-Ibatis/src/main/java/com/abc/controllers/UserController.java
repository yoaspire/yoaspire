package com.abc.controllers;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.ui.ModelMap;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;

import com.abc.bean.UserDetails;
import com.abc.service.ProfileService;


public class UserController extends MultiActionController {
	
	private ProfileService profileService;
	
	public ProfileService getProfileService() {
		return profileService;
	}

	public void setProfileService(ProfileService profileService) {
		this.profileService = profileService;
	}


	public String getForm(HttpServletRequest request, HttpServletResponse response) {
	
		return "regform";		
	}
	
	public String saveUser(HttpServletRequest request, HttpServletResponse response,ModelMap map) {
		
		UserDetails userDetails= new UserDetails();
		userDetails.setFirst_name(request.getParameter("first_name"));
		userDetails.setLast_name(request.getParameter("last_name"));
		userDetails.setEmail(request.getParameter("email"));
		userDetails.setUsername(request.getParameter("username"));
		userDetails.setPassword(request.getParameter("password"));
		userDetails.setOrganization(request.getParameter("organization"));
		userDetails.setPhone(request.getParameter("phone"));
		
		System.out.println(userDetails.getPhone());
		UserDetails user = profileService.saveProfile(userDetails);
		String shwMsg = "some thing went wrong";
		if(user != null) {
			shwMsg ="Registered successfully";
		}
		map.addAttribute("showmsg",shwMsg);
		
		return "success";		
	}
	
	public String edit(HttpServletRequest request, HttpServletResponse response) {
		return "edit";
	}

	public String searchForEdit(HttpServletRequest request, HttpServletResponse response,ModelMap map) {
		
		UserDetails userDetails= new UserDetails();

		userDetails.setUsername(request.getParameter("username"));
		
		
		UserDetails result = profileService.searchProfile(userDetails.getUsername());
		map.addAttribute("userDetails", result);
		map.addAttribute("attempt", 1);
		if (result == null) {
			return "edit";
		}
		return "updatedetails";
	}

	public String updateDetails(HttpServletRequest request, HttpServletResponse response,ModelMap map) {
		UserDetails userDetails= new UserDetails();
		userDetails.setFirst_name(request.getParameter("first_name"));
		userDetails.setLast_name(request.getParameter("last_name"));
		userDetails.setEmail(request.getParameter("email"));
		userDetails.setUsername(request.getParameter("username"));
		userDetails.setPassword(request.getParameter("password"));
		userDetails.setOrganization(request.getParameter("organization"));
		userDetails.setPhone(request.getParameter("phone"));
		
		boolean result = profileService.updateDetails(userDetails);

		map.addAttribute("result", result);
		map.addAttribute("attempt", 1);
		return "updatedetails";
	}

	public String deleteForm(HttpServletRequest request, HttpServletResponse response) {
		return "deleteform";
	}

	public String getDeleteForm(HttpServletRequest request, HttpServletResponse response,ModelMap map) {
		UserDetails userDetails= new UserDetails();

		userDetails.setUsername(request.getParameter("username"));
		int result = profileService.deleteProfile(userDetails.getUsername());
		String showMsg = "";

		if (result != 0)
			showMsg = "deleted  successefully";
		else
			showMsg = "No user found with name " + userDetails.getUsername();

		map.put("deletestatus", showMsg);

		return "deleteform";
	}

	

    
}
