package com.abc.controllers;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;


public class HomeController extends MultiActionController{
	
	
	public String about(HttpServletRequest request,HttpServletResponse  response) {

		System.out.println("about method callled");
		return "about";
	}
}
