
package com.abc.service;

import java.sql.SQLException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.abc.bean.UserDetails;
import com.abc.dao.UserDao;
import com.ibatis.sqlmap.client.SqlMapClient;

public class ProfileServiceImpl implements ProfileService {
	private UserDao userDao;

	public UserDao getUserDao() {
		return userDao;
	}

	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}


	/**
	 * This Method insert new record into databse
	 * @param User Details object
	 */
	public UserDetails saveProfile(UserDetails userDetails) {
		//calling DAO insert method
		UserDetails user=userDao.insert(userDetails);
		return user;
	}

	/**
	 * This Method Search whether recird present in database or not
	 * @param Username
	 * @return User Details object
	 */
	public UserDetails searchProfile(String username) {

		//calling DAO method
		UserDetails user = userDao.getByUserName(username);
		//returning user to controller
		return user;
	}

	/**
	 * If recird is found, this method will update user Info
	 * @param new User Details object
	 * @return boolean
	 */
	public boolean updateDetails(UserDetails userDetails) {

		//getting user id from database
		UserDetails profile = userDao.getById(userDetails.getId());
		boolean status = false;
		
		//Checking given feild is null or not, 
		//if not null then setting new values
		if (userDetails.getEmail() != profile.getEmail()) {
			status = true;
			profile.setEmail(userDetails.getEmail());
		}
		if (userDetails.getFirst_name() != profile.getFirst_name()) {
			status = true;
			profile.setFirst_name(userDetails.getFirst_name());
		}
		if (userDetails.getLast_name() != profile.getLast_name()) {
			status = true;
			profile.setLast_name(userDetails.getLast_name());
		}
		if (userDetails.getOrganization() != profile.getOrganization()) {
			status = true;
			profile.setOrganization(userDetails.getOrganization());
		}
		if (userDetails.getPhone() != profile.getPhone()) {
			status = true;
			profile.setPhone(userDetails.getPhone());
		}
		if (userDetails.getUsername() != profile.getUsername()) {
			status = true;
			profile.setUsername(userDetails.getUsername());
		}
		
		//after setting all values calling DAO update method
		userDao.update(profile);

		return status;
	}

	/**
	 * This will delete record from database
	 * @param user name
	 * @return int value
	 */
	public int deleteProfile(String name) {
		//getting user object from database by username
		UserDetails user = userDao.getByUserName(name);
		
		//calling DAO delete method
		int result = userDao.delete(user);
		return result;

	}


}
