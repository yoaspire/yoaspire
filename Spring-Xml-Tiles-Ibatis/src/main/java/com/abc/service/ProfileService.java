
package com.abc.service;

import com.abc.bean.UserDetails;

public interface ProfileService {

	UserDetails saveProfile(UserDetails userDetails);
	UserDetails searchProfile(String username);
	boolean updateDetails(UserDetails userDetails);
	int deleteProfile(String username);

}

