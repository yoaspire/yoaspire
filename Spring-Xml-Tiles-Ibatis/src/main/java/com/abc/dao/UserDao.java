package com.abc.dao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.abc.bean.UserDetails;
import com.abc.util.SqlMapClientObj;
import com.ibatis.sqlmap.client.SqlMapClient;

/**
 * This Class interacts with databse
 * @author IMVIZAG
 *
 */

public class UserDao {

	/**
	 * This Method inserts new Record in database
	 * @param user
	 * @return user
	 */
	public UserDetails insert(UserDetails user) {
		//getting SqlMapClient object 
		SqlMapClient smc = SqlMapClientObj.getSqlMapClient();
		try {
			
			//calling insert query which is present in xml file
			smc.insert("UserDetails.insert", user);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		//returning user
		return user;
	}

	/**
	 * This method updates existing user record
	 * @param user
	 * @return int value
	 */
	public int update(UserDetails user) {
		//getting SqlMapClient object 
		SqlMapClient smc = SqlMapClientObj.getSqlMapClient();
		int updatedUser = 0;
		try {
			//calling update query which is present in xml file
			updatedUser = smc.update("UserDetails.update", user);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return updatedUser;

	}

	/**
	 * This Method Search User by Id
	 * @param id
	 * @return user
	 */
	public UserDetails getById(int id) {
		//getting SqlMapClient object 
		SqlMapClient smc = SqlMapClientObj.getSqlMapClient();
		UserDetails rec = new UserDetails();
		rec.setId(id);

		List<UserDetails> ems = new ArrayList<UserDetails>();
		try {
			//calling search query which is present in xml file
			ems = (List<UserDetails>) smc.queryForList("UserDetails.findByID", rec);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		//getting first record
		UserDetails user = ems.get(0);
		return user;

	}

	/**
	 * This Method Search User by Username
	 * @param username
	 * @return user
	 */
	public UserDetails getByUserName(String username) {
		
		//getting SqlMapClient object 
		SqlMapClient smc = SqlMapClientObj.getSqlMapClient();
		List<UserDetails> ems = null;

		UserDetails rec = new UserDetails();
		rec.setUsername(username);

		try {
			//calling search query which is present in xml file
			ems = (List<UserDetails>) smc.queryForList("UserDetails.findByUserName", rec);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		//getting first record
		return ems.get(0);
	}

	/**
	 * This Method Deletes Record from database
	 * @param user
	 * @return int value
	 */
	public int delete(UserDetails user) {
		
		//getting SqlMapClient object 
		SqlMapClient smc = SqlMapClientObj.getSqlMapClient();
		int updatedUser = 0;
		try {
			//calling delete query which is present in xml file
			updatedUser = smc.update("UserDetails.delete", user.getId());
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return updatedUser;

	}

}
