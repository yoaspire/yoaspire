package com.abc.dao;


import java.io.IOException;
import java.io.Reader;
import java.sql.SQLException;
import java.util.List;

//import org.hibernate.Query;
//import org.hibernate.Session;
//import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.abc.bean.UserDetails;
import com.ibatis.common.resources.Resources;
import com.ibatis.sqlmap.client.SqlMapClient;
//import com.abc.hibernate.entities.Profile;
import com.ibatis.sqlmap.client.SqlMapClientBuilder;


@Repository
public class ProfileDAOImpl implements ProfileDAO {
	
//	@Autowired
//	private SessionFactory sessionFactory;

	@Override
	public int createProfile(UserDetails userDetails) {
		
		
		
//		Session session=sessionFactory.getCurrentSession();
//		session.save(profile);		
		Reader rd=null;
		try {
			rd = Resources.getResourceAsReader("SqlMapConfig.xml");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	      SqlMapClient smc = SqlMapClientBuilder.buildSqlMapClient(rd);

	      /* This would insert one record in Employee table. */
	      System.out.println("Going to insert record.....");

	      try {
			smc.insert("UserDetails.insert", userDetails);
			return 1;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return 0;
		}
		
	}
	
//	@Override
//	public UserDetails searchProfileByUsername(String username)
//	{
////		Session session=sessionFactory.getCurrentSession();
////		Query query = session.createQuery("from Profile p where p.username=:usename");
////		query.setParameter("usename", username);
////		List<Profile> profiles = (List<Profile>) query.list();
////		Profile profile = null;
////		if(profiles.size()!=0)
////		{
////			profile = profiles.get(0);
////		}
////		else
////		{
//			return null;
////		}
////		UserDetails userDetails = new UserDetails();
////		userDetails.setEmail(profile.getEmail());
////		userDetails.setFirstName(profile.getFirstName());
////		userDetails.setLastName(profile.getLastName());
////		userDetails.setOrganization(profile.getOrganization());
////		userDetails.setPassword(profile.getPassword());
////		userDetails.setPhone(profile.getPhone());
////		userDetails.setUsername(profile.getUsername());
////		userDetails.setId(profile.getId());
////		
////		return userDetails;
//	}
//
//	@Override
//	public Profile findById(int userId) {
////		Session session=sessionFactory.getCurrentSession();
////		Query query = session.createQuery("from Profile p where p.id=:usename");
////		query.setParameter("usename", userId);
////		List<Profile> profiles = (List<Profile>) query.list();
////		if(profiles.size()!=0)
////		{
////			return profiles.get(0);
////		}
////		else
////		{
//			return null;
////		}
//	}
}
