package com.abc.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.abc.bean.UserDetails;
import com.abc.dao.ProfileDAO;
//import com.abc.hibernate.entities.Profile;

@Service
public class ProfileServiceImpl implements ProfileService {
	
	@Autowired
	private ProfileDAO profileDAO;

	@Override
	@Transactional
	public int saveProfile(UserDetails userDetails) {
//		Profile profile = new Profile();
//		profile.setFirstName(userDetails.getFirstName());
//		profile.setLastName(userDetails.getLastName());
//		profile.setUsername(userDetails.getUsername());
//		profile.setPassword(userDetails.getPassword());
//		profile.setOrganization(userDetails.getOrganization());
//		profile.setEmail(userDetails.getEmail());
//		profile.setPhone(userDetails.getPhone());
		return profileDAO.createProfile(userDetails);
	}
	
//	@Override
//	@Transactional
//	public UserDetails searchProfile(String username) {
//		// TODO Auto-generated method stub
//		return profileDAO.searchProfileByUsername(username);
//	}

//	@Override
//	@Transactional
//	public boolean updateDetails(UserDetails userDetails) {
////		Profile profile = profileDAO.findById(userDetails.getId());
//		boolean status = false;
////		if(userDetails.getEmail() != profile.getEmail())
////		{
////			status = true;
////			profile.setEmail(userDetails.getEmail());
////		}
////		if(userDetails.getFirstName() != profile.getFirstName())
////		{
////			status = true;
////			profile.setFirstName(userDetails.getFirstName());
////		}
////		if(userDetails.getLastName() != profile.getLastName())
////		{
////			status = true;
////			profile.setLastName(userDetails.getLastName());
////		}
////		if(userDetails.getOrganization() != profile.getOrganization())
////		{
////			status = true;
////			profile.setOrganization(userDetails.getOrganization());
////		}
////		if(userDetails.getPhone() != profile.getPhone())
////		{
////			status = true;
////			profile.setPhone(userDetails.getPhone());
////		}
////		if(userDetails.getUsername() != profile.getUsername())
////		{
////			status = true;
////			profile.setUsername(userDetails.getUsername());
////		}
////		profileDAO.createProfile(profile);
//		return status;
//	}

}
