<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<form action="editsearch">
Enter user name to update : <input type="text" name="username">
<input type="submit" value="Search">
<c:if test="${userDetails.username != null }">
<jsp:forward page="updatedetails.jsp"></jsp:forward>
</c:if>
<c:if test="${userDetails.username == null && attempt>0}">
<h4>No user found</h4>
</c:if>
</form>
</body>
</html>