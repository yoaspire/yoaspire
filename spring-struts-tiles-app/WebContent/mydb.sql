create table user_details_tbl(id int primary key auto_increment,
first_name varchar(50) not null,
last_name varchar(50),
username varchar(50) not null,
password varchar(50) not null,
organization varchar(50) not null,
email varchar(50) not null,
phone int(10),
CONSTRAINT email_unique UNIQUE (email),
CONSTRAINT username_unique UNIQUE (username));

select * from user_details_tbl;