package com.abc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.abc.service.ProfileService;

@Controller
public class UserDeleteController {
	@Autowired
	ProfileService profileService;
   /**
    * this method returns the deleteform 
    * @return
    */
	@RequestMapping("/deleteform")
	public String getDeleteForm() {
		return "deleteform";
	}
    /**
     * this method is deletes the user from the datebase if user found .
     * @param username
     * @param map
     * @return
     */
	@RequestMapping(value = "/delete")
	public String getDeleteForm(@RequestParam String username,ModelMap map) {
		int result = profileService.deleteProfile(username);
        String showMsg = "";
		
		if (result != 0)
			showMsg = "deleted  successefully";
		else
			showMsg= "No user found with name "+username;
		
		map.put("deletestatus", showMsg);	
		
		//return "deletestatus";
		return "deleteform";
	}
}
