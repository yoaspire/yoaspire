
package com.abc.dao;

import com.abc.bean.UserDetails;
import com.abc.hibernate.entities.Profile;

public interface ProfileDAO {
	
	int createProfile(Profile profile);

	public UserDetails searchProfileByUsername(String username);

	int deleteProfile(String name);
	Profile findById(int userId);
}

