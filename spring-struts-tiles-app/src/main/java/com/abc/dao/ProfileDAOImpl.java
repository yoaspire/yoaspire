
package com.abc.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Repository;

import com.abc.bean.UserDetails;
import com.abc.hibernate.entities.Profile;


@Repository
public class ProfileDAOImpl implements ProfileDAO {
	
	@Autowired
	private SessionFactory sessionFactory;
	

	@Override
	public int createProfile(Profile profile) {
		
		
		
		Session session=sessionFactory.getCurrentSession();
		session.save(profile);		
		
		return profile.getId();
	}
	
	@Override
	public UserDetails searchProfileByUsername(String username)
	{   
		Session session=sessionFactory.getCurrentSession();
		Query query = session.createQuery("from Profile p where p.username=:usename");
		query.setParameter("usename", username);
		List<Profile> profiles = (List<Profile>) query.list();
		Profile profile = null;
		if(profiles.size()!=0)
		{
			profile = profiles.get(0);
		}
		else
		{
			return null;
		}
		UserDetails userDetails = new UserDetails();
		userDetails.setEmail(profile.getEmail());
		userDetails.setFirstName(profile.getFirstName());
		userDetails.setLastName(profile.getLastName());
		userDetails.setOrganization(profile.getOrganization());
		userDetails.setPassword(profile.getPassword());
		userDetails.setPhone(profile.getPhone());
		userDetails.setUsername(profile.getUsername());
		userDetails.setId(profile.getId());
		
		return userDetails;
	}

	
	@Override
	public int deleteProfile(String name) {
		int result = 0;
		Session session=sessionFactory.getCurrentSession();
		Query query = session.createQuery("from Profile p where p.username=:usename");
		query.setParameter("usename", name);
	
		Profile profile = (Profile) query.uniqueResult();
		
		if(profile != null) {
		    session.delete(profile);	
		    result = 1;
		}
		return result;
	}

	@Override
	public Profile findById(int userId) {
		Session session=sessionFactory.getCurrentSession();
		Query query = session.createQuery("from Profile p where p.id=:usename");
		query.setParameter("usename", userId);
		List<Profile> profiles = (List<Profile>) query.list();
		if(profiles.size()!=0)
		{
			return profiles.get(0);
		}
		else
		{
			return null;
		}
	}
}

