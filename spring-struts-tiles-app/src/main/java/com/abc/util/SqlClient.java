package com.abc.util;

import java.io.IOException;
import java.io.Reader;

import com.ibatis.common.resources.Resources;
import com.ibatis.sqlmap.client.SqlMapClient;
import com.ibatis.sqlmap.client.SqlMapClientBuilder;

public class SqlClient {

	public static SqlMapClient getSqlClient() {
		Reader reader;
		SqlMapClient sqlmapClient = null;
		try {
			reader = Resources.getResourceAsReader("sqlmap_config.xml");
			sqlmapClient = SqlMapClientBuilder.buildSqlMapClient(reader);
		} catch (IOException e) {
			e.printStackTrace();
		}

		return sqlmapClient;
	}
}
