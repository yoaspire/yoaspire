
package com.abc.service;

import com.abc.bean.UserDetails;

public interface ProfileService {

	int saveProfile(UserDetails userDetails);
	
	public UserDetails searchProfile(String username);
	int deleteProfile(String name);

	boolean updateDetails(UserDetails userDetails);
}

