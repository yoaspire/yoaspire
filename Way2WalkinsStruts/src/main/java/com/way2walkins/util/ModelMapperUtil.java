package com.way2walkins.util;

import org.modelmapper.ModelMapper;


public class ModelMapperUtil {
 
  private ModelMapper modelMapper = new ModelMapper();
  
  public  <T,R> R convertEntityToDto(T entity,Class<R> type){
	  return modelMapper.map(entity, type);
  }
  
  
}
