package com.way2walkins.util;

import java.io.Serializable;

import java.sql.Connection;
import java.sql.ResultSet;

import java.sql.Statement;

import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.id.IdentifierGenerator;

/**
 * this class is used to create a auto generated values for the all auto increment variables
 * @author IMVIZAG
 *
 */

public class IdGenerator implements IdentifierGenerator {
	
	@Override
	public Serializable generate(SharedSessionContractImplementor session, Object object) throws HibernateException {
		
		Connection con = session.connection();
		Integer newId = 0;
		ResultSet rs = null;
		Statement st = null;
		int prefix = 0;
		try {
			st = con.createStatement();
			switch (object.getClass().getName()) {
			case "com.way2walkins.entities.User":
				prefix = 1001;
				rs = st.executeQuery("select max(user_id) as max from user_tbl");

				break;
				
			case "com.way2walkins.entities.Drive":
				prefix = 2001;
				rs = st.executeQuery("select max(drive_id) as max from drive_tbl");
				break;
				
			case "com.way2walkins.entities.FeedBack":
				prefix = 6001;
				rs = st.executeQuery("select max(feedback_id) as max from feedback_tbl");
				break;
				
			case "com.way2walkins.entities.FeedBackDetails":
				session.flush();
				prefix = 5001;
				rs = st.executeQuery("select max(feedback_details_id) as max from  feedback_details_tbl");
				break;
				
			case "com.way2walkins.entities.Interview":
				prefix = 4001;
				rs = st.executeQuery("select max(interview_id) as max from interview");
				break;
			case "com.way2walkins.entities.Registration":
				prefix = 3001;
				rs = st.executeQuery("select max(reg_id) as max from registration");
				break;
				

			default:
				break;
			}

			if (rs.next()) {
				newId = rs.getInt("max") == 0 ? rs.getInt("max") + prefix : rs.getInt("max") + 1;
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return newId;
	}

}
