package com.way2walkins.service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.way2walkins.dao.DriveDao;
import com.way2walkins.dao.EmployerDao;
import com.way2walkins.dao.InterviewDao;
import com.way2walkins.entities.Drive;
import com.way2walkins.entities.Interview;
import com.way2walkins.entities.User;
import com.way2walkins.util.ModelMapperUtil;
import com.way2walkins.util.RoleEnum;
import com.you_aspire.beens.DriveBasicDto;
import com.you_aspire.beens.DriveInterviews;
import com.you_aspire.beens.InterviewBasicDto;
import com.you_aspire.beens.UserBasicDto;

/**
 * this method gives the drives which are created by this employer
 * 
 * @author IMVIZAG
 *
 */

public class EmployerServiceImpl implements EmployerService {
	

	@Override
	public List<DriveBasicDto> getDrives(int employerId) {
		
		DriveDao driveDao = new DriveDao();
		List<Drive> drivesList = driveDao.findAll();
	    return fillEmployerDrives(drivesList,employerId);
	}

	/**
	 * this method returns the all the candidate feedBack information
	 */
	
public DriveInterviews getInterviewsList(int driveId) {
	
		InterviewDao interviewDAO = new  InterviewDao();
		List<Interview> interviews = interviewDAO.findByDrive(driveId);
		System.out.println(interviews+"hello");
		
		ModelMapperUtil modelMapperUtil = new ModelMapperUtil();
		List<InterviewBasicDto> interviewsList = interviews.stream().map((interview) -> modelMapperUtil.convertEntityToDto(interview, InterviewBasicDto.class)).collect(Collectors.toList());
      
		List<InterviewBasicDto> completedInterviews = new ArrayList<InterviewBasicDto>();
		List<InterviewBasicDto> scheduledInterviews = new ArrayList<InterviewBasicDto>();
		
		for(InterviewBasicDto interview:interviewsList) {
			if(interview.isIs_conducted()) {
				completedInterviews.add(interview);
			}
			else {
				scheduledInterviews.add(interview);
			}
		}
		
		DriveInterviews driveInterviews = new DriveInterviews();
		driveInterviews.setCompletedInterviews(completedInterviews);
		driveInterviews.setScheduledInterviews(scheduledInterviews);
		return driveInterviews;
	}

	@Override
	public List<UserBasicDto> getAllEmployers() throws Exception {
		
		EmployerDao employeeDao = new EmployerDao();
		ModelMapperUtil modelMapperUtil = new ModelMapperUtil();
		List<User> employers = employeeDao.findByRole(RoleEnum.EMPLOYER.getValue());
		List<UserBasicDto> employerBasic = new ArrayList<UserBasicDto>();
		for (User employer : employers) {
			UserBasicDto userBasic = modelMapperUtil.convertEntityToDto(employer, UserBasicDto.class);
			employerBasic.add(userBasic);

		}
		
		return employerBasic;
	}
	
	
	
	@Override
	
	public List<DriveBasicDto> searchByTag(String searchTag, int employerId) throws Exception {
		
		DriveDao driveDao = new DriveDao();
		List<Drive> drivesList = driveDao.findByTag(searchTag);
		return fillEmployerDrives(drivesList, employerId);
 		
	}
	
	
	private List<DriveBasicDto> fillEmployerDrives(Iterable<Drive> drivesList,int employerId){
		
		List<DriveBasicDto> drives = new ArrayList<DriveBasicDto>();

		for (Drive drive : drivesList) {
			if (drive.getEmployers().stream().anyMatch(it -> (it.getUserId() == employerId))) {
				ModelMapperUtil modelMapperUtil = new ModelMapperUtil();
				
				DriveBasicDto driveBasic = modelMapperUtil.convertEntityToDto(drive, DriveBasicDto.class);
				
				drives.add(driveBasic);
			}
		}
		return drives;
	}



 

}
