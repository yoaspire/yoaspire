package com.way2walkins.service;

import java.util.List;

import com.way2walkins.entities.Drive;
import com.way2walkins.entities.FeedBackDetails;
import com.way2walkins.entities.Interview;
//import com.you_aspire.beens.DriveBasicDto;
//import com.you_aspire.beens.UserBasicDto;
import com.way2walkins.util.DriveBasicDto;
import com.way2walkins.util.UserBasicDto;


public interface RecruiterServices {
    
//	public List<DriveBasicDto> getRecruterDrivesList(int recruterId) throws Exception;

	public boolean createDrive(int recruterId, Drive drive)throws Exception;

	public boolean scheduleDrive(int driveId, int candidateId, Interview interview, int recruiterId, int interviwerId)throws Exception;

	public boolean setFeedBackDetails(int driveId, FeedBackDetails feedBackDetails)throws Exception;


	public boolean modifyDrive(Drive drive)throws Exception;

	Interview getScheduleObject(int driveId, int candidateId) throws Exception;

	boolean reScheduleDrive(Interview interview);

//	public List<UserBasicDto> getAllInterviewers()throws Exception;

	public boolean makeDriveComplete(int driveId);

	public List<DriveBasicDto> searchByTag(String searchTag, int recruiterId)throws Exception;

	public List<UserBasicDto> getAllRecruiters()throws Exception;

	public List<DriveBasicDto> getAllRecruiterDrives(int recruiterId);
	
}

