package com.way2walkins.service;

import java.util.List;

import com.you_aspire.beens.DriveBasicDto;
import com.you_aspire.beens.DriveInterviews;
import com.you_aspire.beens.UserBasicDto;

public interface EmployerService {
	
	
	 public List<DriveBasicDto> getDrives(int employerId) throws Exception;
	 
	 public DriveInterviews getInterviewsList(int DriveId)throws Exception;
	 
	 public List<UserBasicDto> getAllEmployers()throws Exception;
	 
	 public List<DriveBasicDto> searchByTag(String searchTag, int employerId)throws Exception;
}
