package com.way2walkins.service;

import com.opensymphony.xwork2.ActionContext;
import com.way2walkins.dao.AuthenticationDao;
import com.way2walkins.entities.User;

public class AuthenticationServiece {
    private AuthenticationDao authenticationDao = null;
	public User login(String emailId,String password) {
	    authenticationDao = new AuthenticationDao();
	    User user = authenticationDao.loginVerify(emailId);
	    ActionContext.getContext().getValueStack().set("userId", user.getUserId());
	    if(user != null && user.getPassword().equals(password)) {
	    	return user;
	    }
	    return null;
		
	}
	
	
}
