package com.way2walkins.service;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.Column;
import javax.transaction.Transactional;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;

import org.modelmapper.ModelMapper;

import com.way2walkins.dao.CandidateDao;
import com.way2walkins.dao.DriveDao;
import com.way2walkins.dao.InterviewDao;
import com.way2walkins.dao.InterviewDaoImpl;
//import com.way2walkins.dao.CandidateDAO;
//import com.way2walkins.dao.DriveDao;
//import com.way2walkins.dao.InterviewDAO;
import com.way2walkins.entities.Drive;
import com.way2walkins.entities.FeedBackDetails;
import com.way2walkins.entities.Interview;
import com.way2walkins.entities.Registration;
import com.way2walkins.entities.User;
import com.way2walkins.util.DriveBasicDto;
import com.way2walkins.util.ModelMapperUtil;
import com.way2walkins.util.RoleEnum;
//import com.way2walkins.util.ModelMapperUtil;
//import com.way2walkins.util.RoleEnum;
//import com.you_aspire.beens.DriveBasicDto;
//import com.you_aspire.beens.UserBasicDto;
import com.way2walkins.util.UserBasicDto;


public class RecruiterServicesImp implements RecruiterServices {

	private DriveDao driveDao=null;

	private CandidateDao userDao=null;

	private InterviewDao interviewDao;



	/**
	 * this method is used to create a drive by a recruiter return the status of
	 * boolean type
	 */
	@Override

	public boolean createDrive(int recruterId, Drive drive) {
		System.out.println("Service "+recruterId);
		int driveStatus = 0;
		userDao=new CandidateDao();
		driveDao = new DriveDao();
		try {
			User recruiter = (User) userDao.findById(recruterId);
			drive.setBy(recruiter);
			driveStatus = driveDao.createDrive(drive);
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (driveStatus != 0) {
			return true;
		}
		driveDao=null;
		userDao = null;
		return false;
	}

	@Override
	public boolean makeDriveComplete(int driveId) {
		boolean status = false;
		driveDao = new DriveDao();
		Drive drive = driveDao.findById(driveId);
		drive.setIs_conducted(true);
		int statusDrived = driveDao.createDrive(drive);
		if (statusDrived != 0) {
			status = true;
		}
		driveDao=null;
		return status;
	}

	@Override
	public List<DriveBasicDto> searchByTag(String searchTag, int recruiterId) throws Exception {
		driveDao = new DriveDao();
		List<Drive> tagDrives = driveDao.searchByTags(searchTag);
		ModelMapperUtil mapperUtil = new ModelMapperUtil();
		List<DriveBasicDto> driveBasicDtos = new ArrayList<>();
		for(Drive drive : tagDrives)
		{
			DriveBasicDto driveBasicDto= mapperUtil.convertEntityToDto(drive, DriveBasicDto.class);
			driveBasicDtos.add(driveBasicDto);
		}
		driveDao=null;
		return driveBasicDtos;

	}

	/**
	 * this method is used to modify the particular drive created by the recruiter
	 */
	@Override
	public boolean modifyDrive(Drive drive) throws Exception {
		driveDao= new DriveDao();
		int driveId = driveDao.createDrive(drive);
		driveDao=null;
		return driveId!=0;

	}


	@Override
	public List<UserBasicDto> getAllRecruiters() throws Exception {

		userDao = new CandidateDao();
		List<User> recruiters = userDao.findByRole(RoleEnum.RECRUITER.getValue());
		ModelMapperUtil modelMapperUtil = new ModelMapperUtil();
		List<UserBasicDto> recruitersBasic = new ArrayList<UserBasicDto>();
		for (User recruiter : recruiters) {
			UserBasicDto userBasic = modelMapperUtil.convertEntityToDto(recruiter, UserBasicDto.class);
			recruitersBasic.add(userBasic);

		}
		return recruitersBasic;
	}





	ModelMapperUtil modelMapperUtil=  new ModelMapperUtil();


	@Override
	public List<DriveBasicDto> getAllRecruiterDrives(int recruiterId) {

		driveDao = new DriveDao();
		List<Drive> driveList = driveDao.getAllRecruiterDrives(recruiterId);
		System.out.println("Size"+driveList.size());

		List<DriveBasicDto> drives = new ArrayList<DriveBasicDto>();

		for (Drive drive : driveList) {
			System.out.println(recruiterId);
			if (drive.getBy().getUserId() == recruiterId) {


				DriveBasicDto driveBasic = modelMapperUtil.convertEntityToDto(drive, DriveBasicDto.class);
				drives.add(driveBasic);
			}
		}


		return drives;
	}



	public boolean scheduleDrive(int driveId, int candidateId, Interview interview, int recruiterId,int interviewerId) {
		driveDao =new DriveDao();
		userDao = new CandidateDao();
		boolean status = false;
		Drive drive = driveDao.findById(driveId);

		List<Registration> registrationList = drive.getRegistrations();

		for (Registration registration : registrationList) {
			if (registration.getBy().getUserId() == candidateId) {
				registration.setIs_scheduled(true);
				status = true;
			}
		}

		User user = userDao.findById(candidateId);
		User recruiter = userDao.findById(recruiterId);
		User interviwer = userDao.findById(interviewerId);
		drive.setDriveId(driveId);

		System.out.println("Interview Object-->"+interview);
		interview.setCandidate(user);
		interview.setInterviewer(interviwer);
		interview.setDrive(drive);
		interview.setRecruiter(recruiter);
		interviewDao = new InterviewDaoImpl();
		int  interviewStatus = interviewDao.save(interview);
		boolean is_interviewSaved = false;
		if(interviewStatus != 0) {
			is_interviewSaved = true;
		}

		driveDao = null;
		userDao=null;
		interviewDao= null;
		return status && is_interviewSaved;

	}

	@Override
	public Interview getScheduleObject(int driveId, int candidateId) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean reScheduleDrive(Interview interview) {
		interviewDao = new InterviewDaoImpl();
		int interviewStatus = interviewDao.save(interview);
		return interviewStatus != 0;
	}

	public boolean setFeedBackDetails(int driveId, FeedBackDetails feedBackDetails) {


		driveDao = new DriveDao();


		Drive drive = driveDao.findById(driveId);
		List<FeedBackDetails> feedBackDetailsList = new ArrayList<>();
		feedBackDetailsList.add(feedBackDetails);
		drive.setFeedbackDetails(feedBackDetailsList);
		boolean driveStatus = driveDao.save(drive);
		driveDao = null;
		return driveStatus;

	}

	public List<UserBasicDto> getAllInterviewers() {

		List<User> interviewrs = userDao.findByRole(RoleEnum.INTERVIEWER.getValue());
		List<UserBasicDto> interviewersBasic = new ArrayList<UserBasicDto>();
		for (User interviewer : interviewrs) {
			UserBasicDto userBasic = modelMapperUtil.convertEntityToDto(interviewer, UserBasicDto.class);
			interviewersBasic.add(userBasic);

		}
		return interviewersBasic;

	}

	


}

