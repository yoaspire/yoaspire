package com.way2walkins.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.way2walkins.entities.User;
import com.way2walkins.util.SessionFactoryUtil;
import com.you_aspire.beens.UserBasicDto;

public class CandidateDao {
	private SessionFactory sessionFactory = null;

	public User findByEmail(String emailId) {
		sessionFactory = SessionFactoryUtil.getSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		Query q = session.createQuery("from  User u where u.email=:emailId");
		q.setParameter("emailId",emailId);
		User user = (User) q.uniqueResult();
		return user;
	}

	public User saveUser(User user)throws Exception {
		
		sessionFactory = SessionFactoryUtil.getSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		
		session.save( user);
		
		transaction.commit();
		session.close();
		return user;
	}

	public User findById(int userId) {
		sessionFactory = SessionFactoryUtil.getSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		Query q = session.createQuery("from  User u where u.userId=:userId");
		q.setParameter("userId",userId);
		User user = (User) q.uniqueResult();
		return user;
	}
	
	public List<User> findByRole(int role)
	{
		sessionFactory = SessionFactoryUtil.getSessionFactory();
		Session session = sessionFactory.openSession();
		
		Query<User> query = (Query<User>) session.createQuery("from User where role=:role");
		query.setParameter("role", role);
		List<User> users = (List<User>) query.getResultList();
		return users;
	}

	
}
