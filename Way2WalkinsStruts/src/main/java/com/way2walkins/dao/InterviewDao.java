package com.way2walkins.dao;

import com.way2walkins.entities.Interview;

public interface InterviewDao {

	public int createInterview(Interview interview);

	public int  save(Interview interview);
	
}
