package com.way2walkins.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.way2walkins.entities.Drive;
import com.way2walkins.entities.Registration;
import com.way2walkins.util.SessionFactoryUtil;

public class DriveDao {
	
	private SessionFactory sessionFactory = null;
	
	public int createDrive(Drive drive)
	{
		sessionFactory = SessionFactoryUtil.getSessionFactory();
		Session session = sessionFactory.openSession();
		
		Transaction transaction = session.beginTransaction();
		
		int driveReturnId=(int) session.save(drive);
		
		System.out.println("dao invoked");
		
		transaction.commit();
		session.close();
		return driveReturnId;
		
	}
	
	public List<Drive> searchByTags(String tag)
	{
		sessionFactory = SessionFactoryUtil.getSessionFactory();
		Session session = sessionFactory.openSession();
		
		Query<Drive> query = session.createQuery("FROM Drive WHERE tags like :tag");
		query.setParameter("tag", "%"+tag+"%");
		session.close();
		return query.getResultList();
	}

	public Drive findById(int driveId) {
		
		System.out.println(driveId);
		sessionFactory = SessionFactoryUtil.getSessionFactory();
		Session session = sessionFactory.openSession();
		
		Drive drive = session.get(Drive.class, driveId);
		session.close();
		return drive;
	}

	public List<Registration> getRegistrations(int driveId) {
		
		sessionFactory = SessionFactoryUtil.getSessionFactory();
		
		Session session = sessionFactory.openSession();
		
		
		Query<Registration> query = session.createQuery("from Registration r where r.drive.driveId=:driveId");
		
		query.setParameter("driveId", driveId);
		
		List<Registration> registrations = new ArrayList<>();
		registrations = query.getResultList();
		
		System.out.println(registrations);
		
//		transaction.commit();
		session.close();
		
		return registrations;
	}
	public List<Drive> getAllRecruiterDrives(int recruiterId) {
		 sessionFactory = SessionFactoryUtil.getSessionFactory();
		Session session = sessionFactory.openSession();
		Query query = (Query) session.createQuery("from Drive d where d.by.userId=:recruiterId");
		query.setParameter("recruiterId", recruiterId);
		 List<Drive> driveList = query.list();
		 session.close();
		return driveList;	
	}

	public boolean save(Drive drive) {
		 sessionFactory = SessionFactoryUtil.getSessionFactory();
			Session session = sessionFactory.openSession();
			Transaction transaction = session.beginTransaction();
			System.out.println("Dao");
			int  driveSaved =  (int) session.save(drive);
			transaction.commit();
			session.close();
			if(driveSaved!=0)
				return true;
			else
				return false;
			
	}
	
	}
