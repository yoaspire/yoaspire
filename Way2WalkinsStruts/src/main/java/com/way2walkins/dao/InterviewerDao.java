package com.way2walkins.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.way2walkins.entities.Interview;
import com.way2walkins.entities.User;
import com.way2walkins.util.SessionFactoryUtil;

public class InterviewerDao {

	private SessionFactory sessionFactory = null;

	public User findUserById(int interviewer_id) {
		sessionFactory = SessionFactoryUtil.getSessionFactory();
		Session session = sessionFactory.openSession();
		User user = (User) session.get(User.class, interviewer_id);
		return user;
	}

	public List<Interview> findByInterviewer(int interviewer_id) {
		sessionFactory = SessionFactoryUtil.getSessionFactory();
		Session session = sessionFactory.openSession();
		Query query = session.createQuery("From Interview i WHERE i.interviewer.userId = :interviewer_user_id");
		query.setParameter("interviewer_user_id", interviewer_id);
		List<Interview> list = query.list();
		return list;
	}

	public Interview findByInterviewById(int interview_id) {
		sessionFactory = SessionFactoryUtil.getSessionFactory();
		Session session = sessionFactory.openSession();
		Interview interview = (Interview) session.get(Interview.class, interview_id);
		return interview;
	}

	public List<Interview> findAll() {

		List<Interview> allInterviews = null;

		sessionFactory = SessionFactoryUtil.getSessionFactory();
		Session session = sessionFactory.openSession();

//		Transaction transaction = session.beginTransaction();
		try {
			allInterviews = session.createQuery("from Interview").list();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return allInterviews;
	}
}
