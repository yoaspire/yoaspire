package com.way2walkins.dao;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transaction;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.way2walkins.entities.Drive;
import com.way2walkins.entities.Interview;
import com.way2walkins.entities.User;
import com.way2walkins.util.SessionFactoryUtil;

public class EmployerDao {
	
	private SessionFactory sessionFactory = null;

	public List<User> findByRole(int role) {
		
		List<User> employers = new ArrayList<>();
		sessionFactory = SessionFactoryUtil.getSessionFactory();
		Session session = sessionFactory.openSession();
		
		Query query = session.createQuery("from User i where i.role =:role");
		query.setParameter("role", role);
		employers = query.list();
		return employers;
	}

}
