package com.way2walkins.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.way2walkins.entities.FeedBack;
import com.way2walkins.util.SessionFactoryUtil;

public class FeedBackDAO {

	private SessionFactory sessionFactory = null;
	
	public FeedBack saveFeedback(FeedBack feedBack) {
		sessionFactory = SessionFactoryUtil.getSessionFactory();
		Session session = sessionFactory.openSession();
		FeedBack savedFeedBack = null;
		savedFeedBack = (FeedBack) session.save(feedBack);
		return savedFeedBack;
	}
}
