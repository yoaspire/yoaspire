package com.way2walkins.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.way2walkins.entities.Interview;
import com.way2walkins.util.SessionFactoryUtil;

public class InterviewDaoImpl implements InterviewDao{

	private SessionFactory sessionFactory = null;
	
	@Override
	public int createInterview(Interview interview) {
		sessionFactory = SessionFactoryUtil.getSessionFactory();
		Session session = sessionFactory.openSession();
		
		Transaction transaction = session.beginTransaction();
		
		int interviewReturnId=(int) session.save(interview);
		
		System.out.println("dao invoked");
		
		transaction.commit();
		
		return interviewReturnId;
	}

	
	public Interview findById(int interviewId) {
		    sessionFactory = SessionFactoryUtil.getSessionFactory();
			Session session = sessionFactory.openSession();
			Interview interview = session.get(Interview.class, interviewId);
			return interview;
	}
	
	public int save(Interview interview) {
	    sessionFactory = SessionFactoryUtil.getSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		int  interviewStatus = (int) session.save(interview);
		transaction.commit();
		
		return interviewStatus;
}
	
}
