package com.way2walkins.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.way2walkins.entities.User;
import com.way2walkins.util.SessionFactoryUtil;

public class AuthenticationDao {
	private SessionFactory sessionFactory = null;

	public User loginVerify(String emailId) {
		sessionFactory = SessionFactoryUtil.getSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		Query q = session.createQuery("from  User u where u.email=:emailId");
		q.setParameter("emailId",emailId);
		User user = (User) q.uniqueResult();
		return user;
	}
}
