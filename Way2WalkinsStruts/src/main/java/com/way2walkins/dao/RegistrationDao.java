package com.way2walkins.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.way2walkins.entities.Registration;
import com.way2walkins.util.SessionFactoryUtil;

public class RegistrationDao {
	private SessionFactory sessionFactory = null;
	
	public Registration register(Registration registration) {
		
		sessionFactory = SessionFactoryUtil.getSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		
		session.save(registration);
		
		transaction.commit();
		session.close();
		return registration;
	}

}
