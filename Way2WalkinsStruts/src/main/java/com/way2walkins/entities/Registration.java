package com.way2walkins.entities;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;


@Entity
@Table
public class Registration {
	@Id
	@GenericGenerator(name = "IdGen" ,strategy = "com.way2walkins.util.IdGenerator" )
	@GeneratedValue(generator = "IdGen")
	@Column(name = "reg_id")
	private int regId;
	@ManyToOne(targetEntity = Drive.class)
	private Drive drive;

	@ManyToOne(targetEntity = User.class)
	private User by;

	@Column(name = "timestamp")
	private Timestamp regTime;

	@Column(name = "is_scheduled")
	private boolean is_scheduled;

	@Column(name = "is_accepted")
	private boolean is_accepted;
	
	public int getRegId() {
		return regId;
	}

	public void setRegId(int regId) {
		this.regId = regId;
	}

	public Drive getDrive() {
		return null;
	}

	public void setDrive(Drive drive) {
		this.drive = drive;
	}

	public User getBy() {
		return by;
	}

	public void setBy(User by) {
		this.by = by;
	}

	public Timestamp getRegTime() {
		return regTime;
	}

	public void setRegTime(Timestamp regTime) {
		this.regTime = regTime;
	}

	public boolean getIs_scheduled() {
		return is_scheduled;
	}

	public void setIs_scheduled(boolean is_scheduled) {

		this.is_scheduled = is_scheduled;
	}

	public boolean getIs_accepted() {
		return is_accepted;
	}

	public void setIs_accepted(boolean is_accepted) {
		this.is_accepted = is_accepted;
	}

}
