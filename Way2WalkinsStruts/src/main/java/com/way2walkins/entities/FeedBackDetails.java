package com.way2walkins.entities;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
/**
 * This class implements the feedback details.
 * @author yo_aspire
 *
 */
@Entity
@Table(name = "feedback_details_tbl")
public class FeedBackDetails {
	//feedback table fields and setters and getters are initialized.
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int feedback_details_id;
	@ManyToMany(mappedBy = "feedbackDetails", cascade = { CascadeType.ALL })
	private List<Drive> drives = new ArrayList<Drive>();
	private String skill;
	private int skill_category;
	private int max_rating;
	private int cutoff_rating;
	private int rating;
	private String comment;

	@ManyToOne(cascade = { CascadeType.ALL })
	@JoinColumn(name = "feed_back_feedback_id")
	private FeedBack feedBack;

	public FeedBack getFeedBack() {
		return null;
	}

	public void setFeedBack(FeedBack feedBack) {
		this.feedBack = feedBack;
	}

	public int getFeedback_details_id() {
		return feedback_details_id;
	}

	public void setFeedback_details_id(int feedback_details_id) {
		this.feedback_details_id = feedback_details_id;
	}

	public List<Drive> getDrives() {
		return null;
	}

	public void setDrives(List<Drive> drives) {
		this.drives = drives;
	}

	public String getSkill() {
		return skill;
	}

	public void setSkill(String skill) {
		this.skill = skill;
	}

	public int getSkill_category() {
		return skill_category;
	}

	public void setSkill_category(int skill_category) {
		this.skill_category = skill_category;
	}

	public int getMax_rating() {
		return max_rating;
	}

	public void setMax_rating(int max_rating) {
		this.max_rating = max_rating;
	}

	public int getCutoff_rating() {
		return cutoff_rating;
	}

	public void setCutoff_rating(int cutoff_rating) {
		this.cutoff_rating = cutoff_rating;
	}

	public int getRating() {
		return rating;
	}

	public void setRating(int rating) {
		this.rating = rating;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

}
