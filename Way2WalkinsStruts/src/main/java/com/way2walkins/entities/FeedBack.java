package com.way2walkins.entities;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;

import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
/**
 * This class implements the feedback.
 * @author yo_aspire
 *
 */
@Entity
@Table(name = "feedback_tbl")
public class FeedBack {
	//feedback table fields and setters and getters are initialized.
	@Id
	@GenericGenerator(name = "IdGen" ,strategy = "com.way2walkins.util.IdGenerator" )
	@GeneratedValue(generator = "IdGen")
	private int feedback_id;
	
	private boolean is_recommended;
	
	private String comments;

	private String interview_video;
	
	@OneToOne(mappedBy = "feedBack",cascade = CascadeType.ALL)
	private Interview interview;
	
	@OneToMany(mappedBy = "feedBack",cascade = CascadeType.ALL)
	private List<FeedBackDetails> drill_down_feedback;

	
	public int getFeedback_id() {
		return feedback_id;
	}

	public Interview getInterview() {
		return null;
		
	}

	public void setInterview(Interview interview) {
		this.interview = interview;
	}

	public void setFeedback_id(int feedback_id) {
		this.feedback_id = feedback_id;
	}

	public boolean isIs_recommended() {
		return is_recommended;
	}

	public void setIs_recommended(boolean is_recommended) {
		this.is_recommended = is_recommended;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getInterview_video() {
		return interview_video;
	}

	public void setInterview_video(String interview_video) {
		this.interview_video = interview_video;
	}

	public List<FeedBackDetails> getDrill_down_feedback() {
		return drill_down_feedback;
	}

	public void setDrill_down_feedback(List<FeedBackDetails> drill_down_feedback) {
		this.drill_down_feedback = drill_down_feedback;
	}


}
