package com.way2walkins.entities;
/**
 * this class is used as the hibernate entity
 * @author IMVIZAG
 *
 */

import java.sql.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;


@Entity
@Table(name = "user_tbl")
public class User {
	@Id
	@GenericGenerator(name = "IdGen" ,strategy = "com.way2walkins.util.IdGenerator" )
	@GeneratedValue(generator = "IdGen")
	@Column(name = "user_id")
	private int userId;
	@Column(name = "register_time")
	private String register_time;

	@Column(name = "first_name")
	private String firstName;
	@Column(name = "last_name")
	private String lastName;

	@Column(name = "email")
	private String email;

	@Column(name = "password")
	private String password;

	@Column(name = "role")
	private int role = 1;

	@Column(name = "gender")
	private String gender;

	@Column(name = "phone_no")
	private String phoneNo;

	@Column(name = "date_of_birth")
	private Date dateOfBirth;

	@Column(name = "skill_tags")
	private String skillTags;

	@Column(name = "language_tags")
	private String languageTags;

	@Column(name = "image")
	private String image;
	

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getRegister_time() {
		return register_time;
	}

	public void setRegister_time(String register_time) {
		this.register_time = register_time;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getRole() {
		return role;
	}

	public void setRole(int role) {
		this.role = role;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getPhoneNo() {
		return phoneNo;
	}

	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getSkillTags() {
		return skillTags;
	}

	public void setSkillTags(String skillTags) {
		this.skillTags = skillTags;
	}

	public String getLanguageTags() {
		return languageTags;
	}

	public void setLanguageTags(String languageTags) {
		this.languageTags = languageTags;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

}
