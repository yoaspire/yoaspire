package com.way2walkins.actions;

import java.sql.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;
import org.hibernate.cfg.beanvalidation.ActivationContext;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;
import com.way2walkins.dao.CandidateDao;
import com.way2walkins.entities.Drive;
import com.way2walkins.entities.User;
import com.way2walkins.service.EmployerServiceImpl;
import com.way2walkins.service.EmployerServices;
import com.way2walkins.service.RecruiterServices;
import com.way2walkins.service.RecruiterServicesImp;
import com.way2walkins.util.DriveBasicDto;
import com.way2walkins.util.UserBasicDto;

public class RecuiterAction extends ActionSupport implements ModelDriven<Drive>{
	HttpServletRequest request ; 
	
	HttpSession session=ServletActionContext.getRequest().getSession(false); 
	
	Drive drive=new Drive();
	RecruiterServices recuiterServices;
	String msg;
	int userid;
	int recuiterId;
	
	private int recruiterId;

	List<UserBasicDto> interviewersList=null;
	List<DriveBasicDto> drivesList=null;
	RecruiterServicesImp recruiterServicesImp = new RecruiterServicesImp();

	private Date date;
	
	private int driveId;

	User user=new User();
	

	/**
	 * @return the drive
	 */
	public Drive getDrive() {
		return drive;
	}

	/**
	 * @param drive the drive to set
	 */
	public void setDrive(Drive drive) {
		this.drive = drive;
	}

	/**
	 * @return the recuiterServices
	 */
	public RecruiterServices getRecuiterServices() {
		return recuiterServices;
	}

	/**
	 * @param recuiterServices the recuiterServices to set
	 */
	public void setRecuiterServices(RecruiterServices recuiterServices) {
		this.recuiterServices = recuiterServices;
	}

	/**
	 * @return the msg
	 */
	public String getMsg() {
		return msg;
	}

	/**
	 * @param msg the msg to set
	 */
	public void setMsg(String msg) {
		this.msg = msg;
	}

	/**
	 * @return the userid
	 */
	public int getUserid() {
		return userid;
	}

	/**
	 * @param userid the userid to set
	 */
	public void setUserid(int userid) {
		this.userid = userid;
	}

	/**
	 * @return the recuiterId
	 */
	public int getRecuiterId() {
		return recuiterId;
	}

	/**
	 * @param recuiterId the recuiterId to set
	 */
	public void setRecuiterId(int recuiterId) {
		this.recuiterId = recuiterId;
	}

	/**
	 * @return the recruiterId
	 */
	public int getRecruiterId() {
		return recruiterId;
	}

	/**
	 * @param recruiterId the recruiterId to set
	 */
	public void setRecruiterId(int recruiterId) {
		this.recruiterId = recruiterId;
	}

	/**
	 * @return the interviewersList
	 */
	public List<UserBasicDto> getInterviewersList() {
		return interviewersList;
	}

	/**
	 * @param interviewersList the interviewersList to set
	 */
	public void setInterviewersList(List<UserBasicDto> interviewersList) {
		this.interviewersList = interviewersList;
	}

	/**
	 * @return the drivesList
	 */
	public List<DriveBasicDto> getDrivesList() {
		return drivesList;
	}

	/**
	 * @param drivesList the drivesList to set
	 */
	public void setDrivesList(List<DriveBasicDto> drivesList) {
		this.drivesList = drivesList;
	}

	/**
	 * @return the recruiterServicesImp
	 */
	public RecruiterServicesImp getRecruiterServicesImp() {
		return recruiterServicesImp;
	}

	/**
	 * @param recruiterServicesImp the recruiterServicesImp to set
	 */
	public void setRecruiterServicesImp(RecruiterServicesImp recruiterServicesImp) {
		this.recruiterServicesImp = recruiterServicesImp;
	}

	/**
	 * @return the date
	 */
	public Date getDate() {
		return date;
	}

	/**
	 * @param date the date to set
	 */
	public void setDate(Date date) {
		this.date = date;
	}

	@Override
	public Drive getModel() {
		return drive;
	}

	@Override
	public void validate() {

	}

	public String createDrive() {

		System.out.println("invoked");
		System.out.println(recuiterId);

		recuiterServices = new RecruiterServicesImp();


		drive = (Drive) getModel();


		drive.setTime_stamp((Date) date);

		drive.setWhen(new Date(System.currentTimeMillis()));

		boolean saveCase=false;

		try {
			saveCase=recuiterServices.createDrive(recuiterId, drive);
		} catch (Exception e) {
			e.printStackTrace();
		}
		if(saveCase)
		{
			msg="Drive Created successfully";
			return SUCCESS;
		}
		else
		{
			msg="Drive Creation failed";
			return ERROR;
		}
	}

	public String getDrivesByTags()
	{
		Drive drive = (Drive) getModel();
		recuiterServices = new RecruiterServicesImp();
		try {
			ActionContext.getContext().getValueStack().set("driveTagList", recuiterServices.searchByTag(drive.getTags(), 1001));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return SUCCESS;
	}

	public String getAllRecuiters()
	{

		recuiterServices = new RecruiterServicesImp();
		try {

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			if(recuiterServices.getAllRecruiters().isEmpty())
			{
				return ERROR;
			}
			else
			{
				ActionContext.getContext().getValueStack().set("allRecuiters", recuiterServices.getAllRecruiters());
				return SUCCESS;

			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return ERROR;
		}
	}

	
	public String getAllRecruiterDrives()
	{
		 user=(User) session.getAttribute("loginUser");
		drivesList = recruiterServicesImp.getAllRecruiterDrives(user.getUserId());
		ActionContext.getContext().getValueStack().set("getDrivesList", drivesList);
		
		if(drivesList!=null)
		{

			return SUCCESS;
		}
		else
		{
			return ERROR;
		}

	}


	public String  getAllInterviewers() {
		System.out.println("---------------------------------");
		interviewersList = recruiterServicesImp.getAllInterviewers();
		ActionContext.getContext().getValueStack().set("getInterviewersList", interviewersList);
		for(UserBasicDto user :interviewersList)
		{ 

			System.out.println("userInfo"+user.getFirstName());
		}
		if(interviewersList!=null)
		{
			return SUCCESS;
		}
		else
		{
			return ERROR;
		}
	}

	public String getDriveDetails() {
		user=(User) session.getAttribute("loginUser");
		recuiterServices = new RecruiterServicesImp();
		try {
			Drive drive = recruiterServicesImp.getDrive(driveId,user.getUserId());
			ActionContext.getContext().getValueStack().set("drive", drive);
			if (drive != null) {
				return "success";
			} else {
				return "error";
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "error";
		}
	}

}
