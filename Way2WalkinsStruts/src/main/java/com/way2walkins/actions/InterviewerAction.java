package com.way2walkins.actions;

import java.util.List;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

import com.way2walkins.entities.Interview;
import com.way2walkins.service.InterviewerService;
import com.way2walkins.service.InterviewerServiceImpl;
import com.you_aspire.beens.InterviewDataForDrive;
import com.you_aspire.beens.InterviewerScheduledData;

public class InterviewerAction extends ActionSupport {

	private int interviewer_id;
	private int interview_id;
	private Interview interview;
	private String searchTag;
	private InterviewerScheduledData data;
	InterviewerService interviewerService = null;

	public String getAllInterviews() {
		interviewerService = new InterviewerServiceImpl();
		data = interviewerService.getAllInterviews(interviewer_id);
		List<InterviewDataForDrive> completedList = data.getCompletedInterviews();
		List<InterviewDataForDrive> scheduledList = data.getScheduledInterviews();
		if (data != null) {
			ActionContext.getContext().getValueStack().set("completedList", completedList);
			ActionContext.getContext().getValueStack().set("scheduledList", scheduledList);
			return ActionSupport.SUCCESS;
		} else {
			return ActionSupport.ERROR;
		}
	}


	public String getAllInterviewByInterviewId() throws Exception {
		interviewerService = new InterviewerServiceImpl();
		interview = interviewerService.getInterviewDetails(interview_id);
		System.out.println(interview.getRecruiter().getFirstName());
		if (interview != null) {
			ActionContext.getContext().getValueStack().set("interview", interview);
			return ActionSupport.SUCCESS;
		} else {
			return ActionSupport.ERROR;
		}
	}

	public String getInterviewsByTag() {
		loginUser = (User) session.getAttribute("loginUser");
		interviewerService = new InterviewerServiceImpl();
		InterviewerScheduledData interviewerScheduledDataTag = null;
		try {
			interviewerScheduledDataTag = interviewerService.searchByTag(searchTag,loginUser.getUserId());
			List<InterviewDataForDrive> completedList = interviewerScheduledDataTag.getCompletedInterviews();
			List<InterviewDataForDrive> scheduledList = interviewerScheduledDataTag.getScheduledInterviews();
			ActionContext.getContext().getValueStack().set("completedList", completedList);
			ActionContext.getContext().getValueStack().set("scheduledList", scheduledList);
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (interviewerScheduledDataTag != null) {
			return ActionSupport.SUCCESS;
		} else {
			return ActionSupport.ERROR;
		}
	}

	public Interview getInterview() {
		return interview;
	}

	public void setInterview(Interview interview) {
		this.interview = interview;
	}

	public int getInterviewer_id() {
		return interviewer_id;
	}

	public void setInterviewer_id(int interviewer_id) {
		this.interviewer_id = interviewer_id;
	}

	public int getInterview_id() {
		return interview_id;
	}

	public void setInterview_id(int interview_id) {
		this.interview_id = interview_id;
	}

	public String getSearchTag() {
		return searchTag;
	}

	public void setSearchTag(String searchTag) {
		this.searchTag = searchTag;
	}

}
