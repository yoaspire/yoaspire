package com.way2walkins.actions;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;
import com.way2walkins.entities.Interview;
import com.way2walkins.service.RecruiterServices;
import com.way2walkins.service.RecruiterServicesImp;

public class ScheduleInterviewAction extends ActionSupport implements ModelDriven<Interview>{
	
	

	public Interview getInterview() {
		return interview;
	}

	public void setInterview(Interview interview) {
		this.interview = interview;
	}
	Interview interview=new Interview();
	
	private RecruiterServices recuiterServices;
	
	private String msg;

	public String getMsg() {
		return msg;
	}

	public RecruiterServices getRecuiterServices() {
		return recuiterServices;
	}

	public void setRecuiterServices(RecruiterServices recuiterServices) {
		this.recuiterServices = recuiterServices;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	@Override
	public Interview getModel() {
		return interview;
	}
	
	private int driveId;
	private int candidateId;
	private int recruiterId;
	private int interviwerId;

	/**
	 * @return the interviwerId
	 */
	public int getInterviwerId() {
		return interviwerId;
	}

	/**
	 * @param interviwerId the interviwerId to set
	 */
	public void setInterviwerId(int interviwerId) {
		this.interviwerId = interviwerId;
	}

	public int getDriveId() {
		return driveId;
	}

	public void setDriveId(int driveId) {
		this.driveId = driveId;
	}

	public int getCandidateId() {
		return candidateId;
	}

	public void setCandidateId(int candidateId) {
		this.candidateId = candidateId;
	}

	public int getRecruiterId() {
		return recruiterId;
	}

	public void setRecruiterId(int recruiterId) {
		this.recruiterId = recruiterId;
	}
	
	public String scheduleInterview()
	{
		recuiterServices = new RecruiterServicesImp();
		System.out.println("Interview===="+interviwerId);
		boolean status = false;
		try {
			status = recuiterServices.scheduleDrive(driveId, candidateId, interview, recruiterId,interviwerId);
		} catch (Exception e) {
			e.printStackTrace();
			status = false;
		}
		
		
		
		if(status)
		{
			msg="Schedule Successfully";
			return SUCCESS;
		}
		else
		{
			msg="Schedule failure";
			return ERROR;
		}
	}
}
