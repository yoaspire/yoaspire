package com.way2walkins.actions;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;
import com.way2walkins.entities.Drive;
import com.way2walkins.entities.User;
import com.way2walkins.service.CandiateServicesImpl;
import com.way2walkins.service.EmployerService;
import com.way2walkins.service.EmployerServiceImpl;
import com.you_aspire.beens.DriveBasicDto;
import com.you_aspire.beens.DriveInterviews;
import com.you_aspire.beens.InterviewBasicDto;
import com.you_aspire.beens.UserBasicDto;

public class EmployerAction extends ActionSupport  {

	private EmployerService employerService = null;
	HttpSession session=ServletActionContext.getRequest().getSession(false); 
	DriveBasicDto driveDTO = new DriveBasicDto();
	DriveInterviews allInterviews = null;
	List<UserBasicDto> allEmployers = null;
	private int employerId;
    private int driveId;
    private String searchTag;
    User loginUser=new User();
    CandiateServicesImpl candiateServicesImpl = new CandiateServicesImpl();
    
	/**
	 * @return the searchTag
	 */
	public String getSearchTag() {
		return searchTag;
	}

	/**
	 * @param searchTag the searchTag to set
	 */
	public void setSearchTag(String searchTag) {
		this.searchTag = searchTag;
	}

	/**
	 * @return the driveId
	 *
	public int getDriveId() {
		return driveId;
	}

	/**
	 * @param driveId the driveId to set
	 */
	public void setDriveId(int driveId) {
		this.driveId = driveId;
	}

	/**
	 * @return the employerId
	 */
	public int getEmployerId() {
		return employerId;
	}

	/**
	 * @param employerId the employerId to set
	 */
	public void setEmployerId(int employerId) {
		this.employerId = employerId;
	}

//	@Override
//	public DriveBasicDto getModel() {
//		// TODO Auto-generated method stub
//		return driveDTO;
//	}
	
	public String getDrivesByEmployer() {
	
		loginUser=(User) session.getAttribute("loginUser");
		employerService = new EmployerServiceImpl();
		List<DriveBasicDto> allDrives = null;
		try {
			allDrives = employerService.getDrives(loginUser.getUserId());
			ActionContext.getContext().getValueStack().set("drivesByEmployer", allDrives);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(allDrives!=null) {
			return "success" ;
		}
		else {
			return "error";
		}
	}
	public String getInterviewsByDrive() {
		
		employerService = new EmployerServiceImpl();
		try {
			allInterviews =  employerService.getInterviewsList(driveId);
			ActionContext.getContext().getValueStack().set("interviews", allInterviews);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(allInterviews!=null) {
			return "success" ;
		}
		else {
			return "error";
		}
	}
	
	public String getAllEmployers() {
		
		employerService = new EmployerServiceImpl();
		try {
			allEmployers =  employerService.getAllEmployers();
			ActionContext.getContext().getValueStack().set("Employers", allEmployers);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(allEmployers!=null) {
			return "success" ;
		}
		else {
			return "error";
		}
	}
	
	public String getDrivesByTag() {
		
		employerService = new EmployerServiceImpl();
		List<DriveBasicDto> allDrives = null;
		try {
			allDrives = employerService.searchByTag(searchTag, employerId);
			ActionContext.getContext().getValueStack().set("drivesByTag", allDrives);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(allDrives!=null) {
			return "success" ;
		}
		else {
			return "error";
		}
	}
	
	public String getDrive() {
		loginUser=(User) session.getAttribute("loginUser");
		
		try {
			Drive drive = candiateServicesImpl.getDrive(driveId,loginUser.getUserId());
			ActionContext.getContext().getValueStack().set("drive", drive);
			if (drive != null) {
				return "success";
			} else {
				return "error";
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "error";
		}
	}
}
