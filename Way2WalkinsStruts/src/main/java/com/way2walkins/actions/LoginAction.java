package com.way2walkins.actions;

import java.util.Map;

import org.apache.struts2.dispatcher.SessionMap;
import org.apache.struts2.interceptor.SessionAware;

import com.opensymphony.xwork2.ActionSupport;
import com.way2walkins.entities.User;
import com.way2walkins.service.AuthenticationServiece;
import com.way2walkins.util.RoleEnum;

public class LoginAction extends ActionSupport  implements SessionAware {
	private AuthenticationServiece authenticationServiece = null;
	
	private SessionMap<String,Object> sessionMap;  
	
	private String emailId;
	private String password;
	
	@Override
	public void setSession(Map<String, Object> session) {
		sessionMap=(SessionMap)session;  
		
	}


	public String login() {
		authenticationServiece = new AuthenticationServiece();
		User user = authenticationServiece.login(emailId, password);
		System.out.println(user);
		if (user != null) {
//			HttpServletRequest request = ServletActionContext.getRequest();
//			request.setAttribute("loginUser", user);
			sessionMap.put("loginUser", user);
			
			switch(user.getRole()) {
			case 1:
				return "candidate";
				
			case 2:
				return "employer";
			case 3:
				return "interviewer";
			case 4:
				return "recruiter";
			}
			return "success";
		} else {
			return "error";
		}
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	
}