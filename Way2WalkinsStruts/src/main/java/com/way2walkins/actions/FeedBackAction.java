package com.way2walkins.actions;

import java.util.ArrayList;
import java.util.List;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;
import com.way2walkins.entities.FeedBack;
import com.way2walkins.entities.FeedBackDetails;
import com.way2walkins.entities.Interview;
import com.way2walkins.service.InterviewerServiceImpl;

public class FeedBackAction {

	FeedBack feedBack = new FeedBack();
	private int driveId;
	private int userId;
	private int interviewerId;
	private int feedback_id;
	private boolean is_recommended;
	private String feedBackComment;
	private String interview_video;
	private int inerviewId;
	private String skill;
	private int skill_category;
	private int max_rating;
	private int cutoff_rating;
	private int rating;
	private String feedbackDetailsComment;
	private int feedback_details_id;
	InterviewerServiceImpl interview = null;
	List<FeedBackDetails> feedBackDetailsList = null;
	public String saveFeedBack() {
		FeedBack feedBack = new FeedBack();
		FeedBackDetails feedBackDetails = new FeedBackDetails();
		feedBack.setComments(feedBackComment);
		feedBack.setFeedback_id(feedback_id);
//		feedBack.getInterview().setInterviewId(inerviewId);
		feedBack.setInterview_video(interview_video);
		feedBack.setIs_recommended(is_recommended);
		//
		feedBackDetails.setComment(feedBackComment);
		feedBackDetails.setCutoff_rating(cutoff_rating);
		feedBackDetails.setFeedBack(feedBack);
		feedBackDetails.setFeedback_details_id(feedback_details_id);
		feedBackDetails.setMax_rating(max_rating);
		feedBackDetails.setRating(rating);
		feedBackDetails.setSkill(skill);
		feedBackDetails.setSkill_category(skill_category);
		feedBackDetailsList = new ArrayList<FeedBackDetails>();
		feedBackDetailsList.add(feedBackDetails);
		feedBack.setDrill_down_feedback(feedBackDetailsList);
		
		interview = new InterviewerServiceImpl();
		boolean result = false;
		result = interview.giveFeedBack(driveId, userId, interviewerId, feedBack);
		if(result) {
			return ActionSupport.SUCCESS;
		}else {
			return ActionSupport.ERROR;
		}
	}
	public int getDriveId() {
		return driveId;
	}
	public void setDriveId(int driveId) {
		this.driveId = driveId;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public int getInterviewerId() {
		return interviewerId;
	}
	public void setInterviewerId(int interviewerId) {
		this.interviewerId = interviewerId;
	}
	public FeedBack getFeedBack() {
		return feedBack;
	}
	public void setFeedBack(FeedBack feedBack) {
		this.feedBack = feedBack;
	}
	public int getFeedback_id() {
		return feedback_id;
	}
	public void setFeedback_id(int feedback_id) {
		this.feedback_id = feedback_id;
	}
	public boolean isIs_recommended() {
		return is_recommended;
	}
	public void setIs_recommended(boolean is_recommended) {
		this.is_recommended = is_recommended;
	}
	public String getFeedBackComment() {
		return feedBackComment;
	}
	public void setFeedBackComment(String feedBackComment) {
		this.feedBackComment = feedBackComment;
	}
	public String getInterview_video() {
		return interview_video;
	}
	public void setInterview_video(String interview_video) {
		this.interview_video = interview_video;
	}
	public int getInerviewId() {
		return inerviewId;
	}
	public void setInerviewId(int inerviewId) {
		this.inerviewId = inerviewId;
	}
	public String getSkill() {
		return skill;
	}
	public void setSkill(String skill) {
		this.skill = skill;
	}
	public int getSkill_category() {
		return skill_category;
	}
	public void setSkill_category(int skill_category) {
		this.skill_category = skill_category;
	}
	public int getMax_rating() {
		return max_rating;
	}
	public void setMax_rating(int max_rating) {
		this.max_rating = max_rating;
	}
	public int getCutoff_rating() {
		return cutoff_rating;
	}
	public void setCutoff_rating(int cutoff_rating) {
		this.cutoff_rating = cutoff_rating;
	}
	public int getRating() {
		return rating;
	}
	public void setRating(int rating) {
		this.rating = rating;
	}
	public String getFeedbackDetailsComment() {
		return feedbackDetailsComment;
	}
	public void setFeedbackDetailsComment(String feedbackDetailsComment) {
		this.feedbackDetailsComment = feedbackDetailsComment;
	}
	public int getFeedback_details_id() {
		return feedback_details_id;
	}
	public void setFeedback_details_id(int feedback_details_id) {
		this.feedback_details_id = feedback_details_id;
	}
	public InterviewerServiceImpl getInterview() {
		return interview;
	}
	public void setInterview(InterviewerServiceImpl interview) {
		this.interview = interview;
	}

}
