package com.way2walkins.actions;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;
import com.way2walkins.entities.FeedBackDetails;
import com.way2walkins.service.RecruiterServicesImp;

@SuppressWarnings("serial")
public class SetFeedBackDetailsAction extends ActionSupport implements ModelDriven<FeedBackDetails>{

	FeedBackDetails feedBackDetails = new FeedBackDetails();
	
	public FeedBackDetails getFeedBackDetails() {
		return feedBackDetails;
	}

	public void setFeedBackDetails(FeedBackDetails feedBackDetails) {
		this.feedBackDetails = feedBackDetails;
	}

	RecruiterServicesImp recruiterServicesImp = new RecruiterServicesImp();
	
	public int getDriveId() {
		return driveId;
	}

	public void setDriveId(int driveId) {
		this.driveId = driveId;
	}

	private int driveId;
	
	
	@Override
	public FeedBackDetails getModel() {
		
		return feedBackDetails;
	}
	
	public String setFeedBackDetails()
	{
		 System.out.println("-----------------------------------------");
		boolean setFeedBackDetailsStatus = recruiterServicesImp.setFeedBackDetails(driveId,feedBackDetails);
	  if(setFeedBackDetailsStatus)
	  {
		  return  SUCCESS;
	  }
	  else
	  {
		 return ERROR;
	  }
		
	}
	

}
