package com.way2walkins.actions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ModelDriven;
import com.way2walkins.entities.Drive;
import com.way2walkins.entities.User;
import com.way2walkins.service.CandiateServicesImpl;
import com.you_aspire.beens.CandidateTotalDrives;
import com.you_aspire.beens.UserBasicDto;

public class CandidateAction implements ModelDriven<Object> {
	
	HttpServletRequest request ; 
	
	HttpSession session=ServletActionContext.getRequest().getSession(false); 

	CandiateServicesImpl candiateServicesImpl = new CandiateServicesImpl();

	UserBasicDto user = new UserBasicDto();
	
	User loginUser=new User();
	

	private int candidateId;

	private int driveId;
	

	private String searchTag;

	public UserBasicDto getUser() {
		return user;
	}

	public void setUser(UserBasicDto user) {
		this.user = user;
	}

	public int getDriveId() {
		return driveId;
	}

	public void setDriveId(int driveId) {
		this.driveId = driveId;
	}

	public int getCandidateId() {
		return candidateId;
	}

	public void setCandidateId(int candidateId) {
		this.candidateId = candidateId;
	}

	public String getSearchId() {
		return searchTag;
	}

	public void setSearchId(String searchId) {
		this.searchTag = searchId;
	}

	@Override
	public UserBasicDto getModel() {
		// TODO Auto-generated method stub
		return user;
	}

	public String register() {

		try {
			User user1 = candiateServicesImpl.candidateRegistration(user);
			if (user1 != null) {

//				ActionContext.getContext().getValueStack().set("user", user1 );
				return "success";
			} else {
				return "error";
			}
		} catch (Exception e) {
			e.printStackTrace();
			return "error";

		}
	}

	public String getCandidateDrives() {
		
		 loginUser=(User) session.getAttribute("loginUser");
			
		try {
			CandidateTotalDrives candidateTotalDrives = candiateServicesImpl.getCandidateDrives(loginUser.getUserId());

			if (candidateTotalDrives != null) {
				ActionContext.getContext().getValueStack().set("drives", candidateTotalDrives);
				return "success";
			} else {
				return "error";
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "error";
		}      
	}

	public String registerDrives() {
		 loginUser=(User) session.getAttribute("loginUser");

		try {
			boolean candidateTotalDrives = candiateServicesImpl.userRegisterToDrive(loginUser.getUserId(), driveId);

			if (candidateTotalDrives) {
				return getCandidateDrives();
			} else {
				return "error";
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "error";
		}
	}

	public String getRegisterDrives() {

		try {
			Drive candidateTotalDrives = candiateServicesImpl.getDrive(driveId, candidateId);
			ActionContext.getContext().getValueStack().set("drives", candidateTotalDrives);
			if (candidateTotalDrives != null) {
				return "success";
			} else {
				return "error";
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "error";
		}
	}

	public String searchByTags() {

		try {
			CandidateTotalDrives candidateTotalDrives = candiateServicesImpl.searchByTag(searchTag, candidateId);
			ActionContext.getContext().getValueStack().set("drives", candidateTotalDrives);
			if (candidateTotalDrives != null) {
				return "success";
			} else {
				return "error";
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "error";
		}
	}
	public String getDrive() {
		loginUser=(User) session.getAttribute("loginUser");
		
		System.out.println(driveId);
		try {
			Drive drive = candiateServicesImpl.getDrive(driveId,loginUser.getUserId());
			ActionContext.getContext().getValueStack().set("user", loginUser);
			ActionContext.getContext().getValueStack().set("drive", drive);
			if (drive != null) {
				return "success";
			} else {
				return "error";
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "error";
		}
	}

}
