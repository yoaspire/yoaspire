package com.you_aspire.beens;

import java.util.List;

import com.way2walkins.entities.Drive;

public class InterviewDataForDrive {

	private Drive drive;
	private List<InterviewBasicDto> interviews;

	public Drive getDrive() {
		return drive;
	}

	public void setDrive(Drive drive) {
		this.drive = drive;
	}

	public List<InterviewBasicDto> getInterviews() {
		return interviews;
	}

	public void setInterviews(List<InterviewBasicDto> interviews) {
		this.interviews = interviews;
	}

}
