package com.you_aspire.beens;


import java.util.List;

public class InterviewerScheduledData {

	private List<InterviewDataForDrive> scheduledInterviews;
	private List<InterviewDataForDrive> CompletedInterviews;

	public List<InterviewDataForDrive> getScheduledInterviews() {
		return scheduledInterviews;
	}

	public void setScheduledInterviews(List<InterviewDataForDrive> scheduledInterviews) {
		this.scheduledInterviews = scheduledInterviews;
	}

	public List<InterviewDataForDrive> getCompletedInterviews() {
		return CompletedInterviews;
	}

	public void setCompletedInterviews(List<InterviewDataForDrive> completedInterviews) {
		CompletedInterviews = completedInterviews;
	}
}

/**
 * this class is giving the full information about the single drive with
 * multiple registrations
 * 
 * @author IMVIZAG
 *
 */


