package com.you_aspire.beens;

import java.util.List;

public class DriveInterviews {
	private List<InterviewBasicDto> completedInterviews;
	private List<InterviewBasicDto> scheduledInterviews;

	public List<InterviewBasicDto> getCompletedInterviews() {
		return completedInterviews;
	}

	public void setCompletedInterviews(List<InterviewBasicDto> completedInterviews) {
		this.completedInterviews = completedInterviews;
	}

	public List<InterviewBasicDto> getScheduledInterviews() {
		return scheduledInterviews;
	}

	public void setScheduledInterviews(List<InterviewBasicDto> scheduledInterviews) {
		this.scheduledInterviews = scheduledInterviews;
	}

}
