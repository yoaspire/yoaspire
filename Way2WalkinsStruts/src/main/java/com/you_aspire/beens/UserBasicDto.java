package com.you_aspire.beens;

import java.util.Date;

public class UserBasicDto {
	
	
	private int userId;
	
	private String register_time;

	private String firstName;
	
	private String lastName;

	private String email;

	private String password;

	private String gender;
	
	private int role = 1;

	private String phoneNo;

	private Date dateOfBirth;

	private String skillTags;

	private String languageTags;

	private String image;

	
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}

	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public int getRole() {
		return role;
	}
	public void setRole(int role) {
		this.role = role;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getPhoneNo() {
		return phoneNo;
	}
	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}
	public Date getDateOfBirth() {
		return dateOfBirth;
	}
	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	public String getSkillTags() {
		return skillTags;
	}
	public void setSkillTags(String skillTags) {
		this.skillTags = skillTags;
	}
	public String getLanguageTags() {
		return languageTags;
	}
	public void setLanguageTags(String languageTags) {
		this.languageTags = languageTags;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getRegister_time() {
		return register_time;
	}
	public void setRegister_time(String register_time) {
		this.register_time = register_time;
	}
	
}
