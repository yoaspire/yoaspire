package com.you_aspire.beens;

import java.sql.Date;


public class DriveBasicDto {
	private int driveId;
	private UserBasicDto by;
	private String heading;
	private String sub_heading;
	private String description;
	private String image;
	private String url;
	private Date when;
	private boolean isRegistered;
	
	
	
	public int getDriveId() {
		return driveId;
	}
	public void setDriveId(int driveId) {
		this.driveId = driveId;
	}
	public UserBasicDto getBy() {
		return by;
	}
	public void setBy(UserBasicDto by) {
		this.by = by;
	}
	public String getHeading() {
		return heading;
	}
	public void setHeading(String heading) {
		this.heading = heading;
	}
	public String getSub_heading() {
		return sub_heading;
	}
	public void setSub_heading(String sub_heading) {
		this.sub_heading = sub_heading;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public Date getWhen() {
		return when;
	}
	public void setWhen(Date when) {
		this.when = when;
	}
	public boolean isRegistered() {
		return isRegistered;
	}
	public void setRegistered(boolean isRegistered) {
		this.isRegistered = isRegistered;
	}
}
