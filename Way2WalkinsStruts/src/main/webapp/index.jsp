<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Way2Walkins</title>
<link rel="stylesheet" type="text/css" href="./assets/css/login.css">
<link rel = "stylesheet" href ="./css/basestyles.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
</head>
<body>
    <form action="candidate/login" method="post">
    <div class="loginformback"> 
        <img src="./images/logo3.png" ></img>
         <div class="username-password-formholder">
           <div class="username-password-form flex-column">
             <form>
               <p class="subheading-txt pu ">Username:</p>
               <input class='username-txt-field input1' name="emailId" refs="emailId"  type="email"   ></input>
               <p class="subheading-txt pp">Password: </p>
               </form>
               <input class='password-txt-field input1' name="password" refs="password"  type='password'  ></input>
             <input class="button" type="submit" name="submit" value="Login"/>
            </form>             
             <a href="candidate_register.jsp"> <p class="forgot">Don't have an account?Register</p></a> 
           </div>
         </div>         
         </div>
</body>
</html>