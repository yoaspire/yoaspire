<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Way2Walkins</title>
<link rel="stylesheet" type="text/css" href="./assets/css/createDrive.css"/>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
</head>
<body>
	      <div class="main-registration-container2">
        <div classNmae="container-floating">
          <div class="register">
            <div class="row">
              <div class="col-sm-1"></div>
              <div class="col-sm-8">
                <h3 class="drive-heading">Create Drive</h3>


              </div>
              <div class="col-sm-2"></div>

            </div>

            <form method="post" action = "recuiter/createDrive"  >


              <div class="row">
                <div class="col-sm-1"></div>
                <div class="col-sm-8">
                  <label>Heading</label>
                  <input type="text" name="heading" placeholder="Heading" />
                </div>
                <div class="col-sm-2"></div>
              </div>

              <div class="row">
                <div class="col-sm-1"></div>
                <div class="col-sm-8">
                  <label>Sub Heading</label>
                  <input type="text" name=sub_heading placeholder=" Sub Heading" />
                </div>
                <div class="col-sm-2"></div>

              </div>
              <div class="row">
                <div class="col-sm-1"></div>
                <div class="col-sm-8">
                  <label>Description</label>
                  <textarea class="description col-sm-11 " type="text" name="description" rows="5" cols="95"></textarea>
                </div>
                <div class="col-sm-2"></div>

              </div>

              <div class="row">
                <div class="col-sm-1"></div>
                <div class="col-sm-8">
                  <label>City</label>
                  <input type="text" name="city" />
                </div>
                <div class="col-sm-2"></div>

              </div>

              <div class="row">
                <div class="col-sm-1"></div>
                <div class="col-sm-8">
                  <label>Venue</label>
                  <input type="text" name="where"/>
                </div>
                <div class="col-sm-2"></div>

              </div>

              <div class="row">
                <div class="col-sm-1"></div>
                <div class="col-sm-8">
                  <label>Exactly</label>
                  <input type="text" name="exactly"/>
                </div>
                <div class="col-sm-2"></div>

              </div>

              <div class="row">
                <div class="col-sm-1"></div>
                <div class="col-sm-8">
                  <label>Date of Drive:</label>
                  <s:textfield name="date" id="date" key="invitation.date" required="true" value="%{getText('format.datetimesecond',{uploadedDate})}" size="8"/>
                </div>
                <div class="col-sm-2"></div>

              </div>


              <div class="row">
                <div class="col-sm-1"></div>
                <div class="col-sm-8">
                  <label>URL:</label>
                  <input type="text" name="url" placeholder="drive URL" />
                </div>
                <div class="col-sm-2"></div>

              </div>

              <div class="row">
                <div class="col-sm-1"></div>
                <div class="col-sm-8">
                  <label>Eligibility:</label>
                  <textarea class="description col-sm-11" type="text" name="eligibility" rows="5" cols="95" ></textarea>

                </div>
                <div class="col-sm-2"></div>

              </div>

              <div class="row">
                <div class="col-sm-1"></div>
                <div class="col-sm-8">
                  <label>More Information:</label>
                  <textarea class="description col-sm-11" type="text" name="more" rows="5" cols="95"></textarea>

                </div>
                <div class="col-sm-2"></div>

              </div>

              <div class="row">
                <div class="col-sm-1"></div>
                <div class="col-sm-8">
                  <label>Mobile No:</label>
                  <input type="text" name="phone" />


                </div>
                <div class="col-sm-2"></div>

              </div>


              <div class="row">
                <div class="col-sm-1"></div>
                <div class="col-sm-8">
                  <label>Tags:</label>
                  <input type="text" name="tags" placeholder="Search tags ex-'name,short name" />
                </div>
                <div class="col-sm-2"></div>

              </div>


            
              <div class="row">
                <div class="col-sm-1"></div>
                <div class="col-sm-6">
                  <input type="submit" class="button1" value="CREATE DRIVE" />
                </div>
                <div class="col-sm-3"></div>

              </div>
            </form>
          </div>
        </div>
      </div>
</body>
</html>