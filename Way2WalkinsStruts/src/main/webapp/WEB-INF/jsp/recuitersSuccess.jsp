<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<table>
<tr>
<th>userId</th>
<th>firstName</th>
<th>email</th>
<th>role</th>
<th>image</th>
</tr>
	<s:iterator value="allRecuiters" id="list">
		<tr style="background-color: #99CCFF">      
            <td><s:property value="#list.userId"/></td>
            <td><s:property value="#list.by.firstName"/></td>
            <td><s:property value="#list.email"/></td>
            <td><s:property value="#list.role"/></td>
            <td><s:property value="#list.image"/></td>
        </tr>
	</s:iterator>
</table>

</body>
</html>