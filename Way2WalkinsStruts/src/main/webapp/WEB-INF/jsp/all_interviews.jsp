<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<style>
table, th, td {
	border: 1px solid black;
	border-collapse: collapse;
	padding: 10px;
}
</style>

<body>
  <nav class="navbar navbar-expand-sm bg-info navbar-dark">
    <ul class="navbar-nav">
      <li class="nav-item ">
        <a class="nav-link" href="#">Logo</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">My profiles</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">My drives</a>
      </li>
      <li class="nav-item">
        <a class="nav-link " href="#">Logout</a>
      </li>
    </ul>
  </nav> <br><br>
  
  <form action="interviewer/getInterviewsByTag">
	Tag To Search: <input type="text" name="searchTag">
	<input class="btn btn-outline-info" type="submit" value="Search">
	
</form>	
  <div class="container">
    <div class="row">
    <s:iterator value="scheduledList">
      <div class="col-md-3 ">
        <div class="card profile-card-5"
          style="box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);">
          <div class="card-body pt-0">
            <h5 class="card-title"></h5>
            
            <p><b>Drive Id: </b><s:property value="getDrive().getDriveId()" /></p>
            <p><b>Created By: </b><s:property value="getDrive().getBy().getFirstName()" /></p>
            <p><b>City: </b><s:property value="getDrive().getCity()" /></p>
            <p><b>Heading: </b><s:property value="getDrive().getHeading()" /></p>
            <p><b>Sub-Heading: </b><s:property value="getDrive().getSub_heading()" /></p>
            <p><b>When: </b><s:property value="getDrive().getWhen()" /></p>
            <p><b>Where: </b><s:property value="getDrive().getWhere()" /></p>
            
          </div>
        </div>
      </div>
      </s:iterator>
    </div>
>>>>>>> 134d465a1dcecd08b9de164c78a595349166a6e2
</body>
</html>