
<%@page import="com.you_aspire.beens.CandidateTotalDrives"%>
<%@ page language="java" contentType="text/html; charset = ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<title>Document</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
</head>
<body>

	<nav class="navbar navbar-expand-sm bg-info navbar-dark">
		<ul class="navbar-nav">
			<li class="nav-item "><a class="nav-link" href="#">Logo</a></li>
			<li class="nav-item"><a class="nav-link" href="#">My
					profiles</a></li>
			<li class="nav-item"><a class="nav-link" href="#">My drives</a>
			</li>
			<li class="nav-item"><a class="nav-link " href="#">Logout</a></li>
		</ul>
	</nav>
	<br>
	<br>
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-1"></div>
			<div class="col-lg-5">
				<div
					style="background-color: #17a2b8; height: 45px; border-radius: 10px">
					<h3 style="text-align: center">Registered drives</h3>
				</div>
				<br>
				<s:iterator value="drives.regiteredDrives" id="regDrive">
					<div name="driveItem"
						style="background-color: #bee9f0; border-radius: 10px; padding: 10px">
						<div class="row">
							<div class="col-lg-3">
								<img src="#regDrive.image" alt="no image" width="100px"
									height="100px" style="border-radius: 60px">
							</div>
							<div class="col-lg-9">
								<p>
									<b> Heading: </b>
									<s:property value="#regDrive.heading" />
								</p>
								<p>
									<b> Date: </b>
									<s:property value="#regDrive.when" />
								</p>
								<p>
									<b>Description: </b>
									<s:property value="#regDrive.description" />
								</p>
								<p>
									<b>location: </b>
									<s:property value="#regDrive.city" />
								</p>
						

								<s:url id="path" action="getDrive">
									<s:param name="driveId" value="#regDrive.driveId" />
								</s:url>
								<button style="float: right"><s:a href="%{path}">Read</s:a></button>


							</div>
						</div>
					</div>
					<br>
				</s:iterator>

			</div>



			<div class="col-lg-5">
				<div
					style="background-color: #17a2b8; height: 45px; border-radius: 10px">
					<h3 style="text-align: center">Suggested drives</h3>
				</div>
				<br>
				<s:iterator value="drives.suggestedDrives" id="sugDrive">
					<div
						style="background-color: #bee9f0; border-radius: 10px; padding: 10px">
						<div class="row">
							<div class="col-lg-3">
								<img src="#sugDrive.image" alt="" width="100px" height="100px"
									style="border-radius: 60px">
							</div>
							<div class="col-lg-9">
								<p>
									<b> Heading: </b>
									<s:property value="#sugDrive.heading" />
								</p>
								<p>
									<b> Date: </b>
									<s:property value="#sugDrive.when" />
								</p>
								<p>
									<b>Description: </b>
									<s:property value="#sugDrive.description" />
								</p>
								<p>
									<b>location: </b>
									<s:property value="#sugDrive.city" />
								</p>
								<s:url id="path" action="getDrive">
									<s:param name="driveId" value="#sugDrive.driveId" />
								</s:url>
								<button style="float: right"><s:a href="%{path}">Read</s:a></button>
							</div>

						</div>
					</div>
					<br>
				</s:iterator>
			</div>




		</div>
	</div>

</body>

</html>

