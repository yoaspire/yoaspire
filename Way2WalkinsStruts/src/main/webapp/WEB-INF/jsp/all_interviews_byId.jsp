<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>

</head>
<style>
table, th, td {
	border: 1px solid black;
	border-collapse: collapse;
	padding: 10px;
}
</style>
<body>
	<table style="width: 100%;">
		<h2>Sheduled Interview Drives</h2>
		<tr>
			<th>Drive Id</th>
			<th>Recruiter Name</th>
			<th>City</th>
			<th>Description</th>
			<th>Eligibility</th>
			<th>Heading</th>
			<th>Sub-Heading</th>
			<th>Image</th>
			<th>When</th>
			<th>Where</th>
		</tr>

		<tr>

			<td><s:property value="interview.getDrive().getDriveId()" /></td>
			<td><s:property value="interview.getRecruiter().getFirstName()" /></td>
			<td><s:property value="interview.getDrive().getCity()" /></td>
			<td><s:property value="interview.getDrive().getDescription()" /></td>
			<td><s:property value="interview.getDrive().getEligibility()" /></td>
			<td><s:property value="interview.getDrive().getHeading()" /></td>
			<td><s:property value="interview.getDrive().getSub_heading()" /></td>
			<td><s:property value="interview.getDrive().getImage()" /></td>
			<td><s:property value="interview.getDrive().getWhen()" /></td>
			<td><s:property value="interview.getDrive().getWhere()" /></td>

		</tr>

	</table>
</body>
</html>