<%@ page language = "java" contentType = "text/html; charset = ISO-8859-1"
   pageEncoding = "ISO-8859-1"%>
<%@ taglib prefix = "s" uri = "/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"
    integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
  <title>Document</title>

  <style>
    .profile-card-5 {
      margin-top: 20px;
    }

    .profile-card-5 .btn {
      border-radius: 2px;
      text-transform: uppercase;
      font-size: 12px;
      padding: 7px 20px;
    }

    .profile-card-5 .card-img-block {
      width: 91%;
      margin: 0 auto;
      position: relative;
      top: -20px;

    }

    .profile-card-5 .card-img-block img {
      border-radius: 5px;
      box-shadow: 0 0 10px rgba(0, 0, 0, 0.63);
    }

    .profile-card-5 h5 {
      color: #17a2b8;
      font-weight: 600;
    }

    .profile-card-5 p {
      font-size: 14px;
      font-weight: 300;
    }

    .profile-card-5 .btn-primary {
      background-color: #17a2b8;
      border-color: #17a2b8;
    }
  </style>

</head>

<body>
  <nav class="navbar navbar-expand-sm bg-info navbar-dark">
    <ul class="navbar-nav">
      <li class="nav-item ">
        <a class="nav-link" href="#">Logo</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">My profiles</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">My drives</a>
      </li>
      <li class="nav-item">
        <a class="nav-link " href="#">Logout</a>
      </li>
    </ul>
  </nav> <br><br>
  <div class="container">
    <div class="row">
    <s:iterator value="scheduledList">
      <div class="col-md-3 ">
        <div class="card profile-card-5"
          style="box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);">
          <div class="card-body pt-0">
            <h5 class="card-title"></h5>
            
            <p><b>Drive Id: </b><s:property value="getDrive().getDriveId()" /></p>
            <p><b>Created By: </b><s:property value="getDrive().getBy().getFirstName()" /></p>
            <p><b>City: </b><s:property value="getDrive().getCity()" /></p>
            <p><b>Heading: </b><s:property value="getDrive().getHeading()" /></p>
            <p><b>Sub-Heading: </b><s:property value="getDrive().getSub_heading()" /></p>
            <p><b>When: </b><s:property value="getDrive().getWhen()" /></p>
            <p><b>Where: </b><s:property value="getDrive().getWhere()" /></p>
            
          </div>
        </div>
      </div>
      </s:iterator>
    </div>
</body>

</html>