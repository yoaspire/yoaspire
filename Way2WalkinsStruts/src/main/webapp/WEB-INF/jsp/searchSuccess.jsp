<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<s:if test="%{getProductList().isEmpty()}">
   Error
</s:if>
<table>
<tr>
<th>Drive Id</th>
<th>Recuiter</th>
<th>Heading</th>
<th>Sub_heading</th>
<th>Description</th>
<th>Eligiblity</th>
<th>url</th>
<th>Phone number</th>
<th>City</th>
<th>Venue</th>
<th>Date and Time of the interview</th>
</tr>
	<s:iterator value="driveTagList" id="list">
		<tr style="background-color: #99CCFF">      
            <td><s:property value="#list.driveId"/></td>
            <td><s:property value="#list.by.firstName"/></td>
            <td><s:property value="#list.heading"/></td>
            <td><s:property value="#list.sub_heading"/></td>
            <td><s:property value="#list.description"/></td>
            <td><s:property value="#list.eligibility"/></td>
            <td><s:a><s:property value="#list.url"/></s:a></td>            
            <td><s:property value="#list.phone"/></td>
            <td><s:property value="#list.city"/></td>
            <td><s:property value="#list.where"/></td>
            <td><s:property value="#list.time_stamp"/></td>                 
        </tr>
	</s:iterator>
</table>
</body>
</html>