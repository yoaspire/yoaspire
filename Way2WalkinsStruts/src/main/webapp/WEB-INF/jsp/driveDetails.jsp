<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Way2Walkins</title>
<link rel="stylesheet" type="text/css" href="./css/readArticle.css">
<link rel="stylesheet" href="./css/basestyles.css">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
</head>
<style>
@import url(https://fonts.googleapis.com/css?family=Roboto:900,300);

.frame {
	width: 90%;
	margin: 80px auto 120px;
	background-image:
		url("https://www.transparenttextures.com/patterns/bright-squares.png");
	padding: 0 20px 20px;
	border-radius: 6px;
	-webkit-border-radius: 6px;
	-moz-border-radius: 6px;
	box-shadow: 0 2px 5px rgba(0, 0, 0, 0.075);
	-webkit-box-shadow: 0 2px 5px rgba(0, 0, 0, 0.075);
	-moz-box-shadow: 0 2px 5px rgba(0, 0, 0, 0.075);
}

.container:hover .avatar-flip {
	transform: rotateY(180deg);
	-webkit-transform: rotateY(180deg);
}

.container:hover .avatar-flip img:first-child {
	opacity: 0;
}

.container:hover .avatar-flip img:last-child {
	opacity: 1;
}

.avatar-flip {
	border-radius: 100px;
	overflow: hidden;
	height: 150px;
	width: 150px;
	position: relative;
	margin: auto;
	top: -60px;
	transition: all 0.3s ease-in-out;
	-webkit-transition: all 0.3s ease-in-out;
	-moz-transition: all 0.3s ease-in-out;
	box-shadow: 0 0 0 13px #f0f0f0;
	-webkit-box-shadow: 0 0 0 13px #f0f0f0;
	-moz-box-shadow: 0 0 0 13px #f0f0f0;
}

.avatar-flip img {
	position: absolute;
	left: 0;
	top: 0;
	border-radius: 100px;
	transition: all 0.3s ease-in-out;
	-webkit-transition: all 0.3s ease-in-out;
	-moz-transition: all 0.3s ease-in-out;
}

.avatar-flip img:first-child {
	z-index: 1;
}

.avatar-flip img:last-child {
	z-index: 0;
	transform: rotateY(180deg);
	-webkit-transform: rotateY(180deg);
	opacity: 0;
}

.profilepic_details {
	border-radius: 20%;
	margin: 1vw;
}

.card-title {
	text-align: center;
	color: #30688d;
	text-transform: capitalize;
}

.card-text b {
	color: #30688d;
}

.card-text {
	font-family: 'Gill Sans', 'Gill Sans MT', Calibri, 'Trebuchet MS',
		sans-serif;
	font-size: 14px;
	color: black;
}

#go_back {
	margin-left: 1020px;
	margin-top: 10px;
}

.btn-completeDrive {
	font-weight: bold;
	margin-top: 7%;
	margin-left: 17%;
}

.btn-editDrive {
	font-weight: bold;
	margin-top: -25%;
	margin-right: 20%;
}

.row-align {
	margin-left: 800px;
}

.completed {
	font-weight: bold;
	color: red;
	font-size: 20px;
}

#team {
	margin-bottom: -10%;
}
</style>
<body>
	<div className="color">
       <div className="container1">
		<h1 className="h1">Drive Details</h1>
		<section id="team" class="pb-5">
			<div class="container1 frame">
				<div class="avatar-flip">
					<img className="img"
						src="https://pi.tedcdn.com/r/talkstar-assets.s3.amazonaws.com/production/playlists/playlist_352/watch_before_a_job_interview_1200x627.jpg?c=1050%2C550&w=1050"
						width="140px" height="140px" /> <img className="img"
						src="https://pi.tedcdn.com/r/talkstar-assets.s3.amazonaws.com/production/playlists/playlist_352/watch_before_a_job_interview_1200x627.jpg?c=1050%2C550&w=1050"
						width="140px" height="140px" />
				</div>
				<h5 className="card-title" style="margin-left: 35%">
					<s:property value="drive.heading" />
				</h5>
				<br />
				<p className="card-text">
					<b>City:&nbsp;</b>
					<s:property value="drive.city" />
				</p>
				<br />
				<p className="card-text">
					<b>Description:&nbsp;</b>
					<s:property value="drive.description" />
				</p>
				<br />
				<p className="card-text">
					<b>Venue:&nbsp;</b>
					<s:property value="drive.where" />
				</p>
				<br />
				<p className="card-text">
					<b>Date and time :&nbsp;</b>
					<s:property value="drive.when" />
				</p>
				<br />
				<p className="card-text">
					<b>Eligibility:&nbsp;</b>
					<s:property value="drive.eligibility" />
				</p>
				<br />
				<p className="card-text">
					<b>More Information :&nbsp;</b>
					<s:property value="drive.more_information" />
				</p>
			</div>
		</section>
		<div>
			<InterviewerDriveFooter driveId={driveid} />
			<div>
				<EmployerDriveFooter driveId={this.props.match.params.id} />

				 <s:if test="user.role == 4"> 
					<div>
						<div>
							<div className="row row-align">

								<div className="col-sm-2">
									<button className="btn-completeDrive">Complete&nbsp;Drive</button>
								</div>
								&emsp;&emsp;

								<div>
									<div className="col-sm-1">
										<a href=""><button className="btn-editDrive">Edit&nbsp;Drive</button></a>
									</div>
								</div>
							</div>
				</s:if>
					 <s:if test="user.role == 1 && !drive.isRegistered"> 
				<s:url id="path" action="registerToDrive">
									<s:param name="driveId" value="drive.driveId" />
								</s:url>
								<button style="margin-left: 45%;background:aqua;"><s:a href="%{path}">Register</s:a></button>
								</s:if>
</body>
</html>