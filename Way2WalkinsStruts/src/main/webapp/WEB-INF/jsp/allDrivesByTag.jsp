<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
     <%@ taglib prefix = "s" uri = "/struts-tags" %>
<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
  <title>Document</title>
  <style>
    .profile-card-5 {
      margin-top: 20px;
    }

    .profile-card-5 .btn {
      border-radius: 2px;
      text-transform: uppercase;
      font-size: 12px;
      padding: 7px 20px;
    }

    .profile-card-5 .card-img-block {
      width: 91%;
      margin: 0 auto;
      position: relative;
      top: -20px;

    }

    .profile-card-5 .card-img-block img {
      border-radius: 5px;
      box-shadow: 0 0 10px rgba(0, 0, 0, 0.63);
    }

    .profile-card-5 h5 {
      color: #17a2b8;
      font-weight: 600;
    }

    .profile-card-5 p {
      font-size: 14px;
      font-weight: 300;
    }

    .profile-card-5 .btn-primary {
      background-color: #17a2b8;
      border-color: #17a2b8;
    }
    .input-group.md-form.form-sm.form-2 input.lime-border {
      border: 1px solid #17a2b8;
    }
  </style>
</head>

<body>

  <nav class="navbar navbar-expand-sm bg-info navbar-dark">
    <ul class="navbar-nav">
      <li class="nav-item ">
        <a class="nav-link" href="#">Logo</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">My profiles</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">My drives</a>
      </li>
      <li class="nav-item">
        <a class="nav-link " href="#">Logout</a>
      </li>
    </ul>
  </nav> <br><br>

<div class="container" >
<div class="input-group md-form form-sm form-2 pl-0">
      <input class="form-control my-0 py-1 lime-border" type="text" placeholder="Search drives" aria-label="Search">
      <div class="input-group-append">
        <span class="input-group-text lime lighten-2" id="basic-text1"><i class="fa fa-search" aria-hidden="true"></i></span>
      </div>
    </div>
    <br><br>
<div class="row">
 <s:iterator value="drivesByTag" id="list">
  <div class="col-md-3 ">
    <div class="card profile-card-5" style="box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);"> 
      <div class="card-img-block">
        <img class="card-img-top"
          src=<s:property value="#list.image"/>
          style="height:200px">
      </div>
      <div class="card-body pt-0">
        <h5 class="card-title">Drive details</h5>
        <p><b>venue:<s:property value="#list.where"/></b></p>
        <p><b>Apply:<s:property value="#list.when"/></b></p>
        <p><b>Last day:<s:property value="#list.register_before"/></b></p>
        <a href="readDrive.jsp" class="btn btn-primary">Read</a>
      </div>
    </div>
  </div>
  </s:iterator>
  </div>
<hr/>
  
  
</div>

</body>

</html>