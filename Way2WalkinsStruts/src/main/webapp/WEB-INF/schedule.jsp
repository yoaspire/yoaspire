<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<s:property value="msg" default="Please enter details"/>
<s:form action="recuiter/scheduleInterview">
RecuiterId:<input type="number" name="recruiterId" /><br><br>
CandidateId:<input type="number" name="candidateId"/><br><br>
DriveId:<input type="number" name="driveId"/><br><br>
InterviewId:<input type="number" name="interviewId"/><br><br>
<s:textfield name="when_interviewed" key="Date" required="true" value="%{getText('format.datetimesecond',{uploadedDate})}"></s:textfield>
<s:textfield name="exactly" key ="exactly"></s:textfield>
<s:submit value="Schedule Interview"></s:submit>
</s:form>
</body>
</html>