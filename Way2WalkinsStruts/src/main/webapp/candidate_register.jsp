<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
  <%@ taglib prefix = "s" uri = "/struts-tags"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Way2Walkins</title>
<link rel="stylesheet" type="text/css" href="./assets/css/createDrive.css">
<link rel = "stylesheet" href ="./css/basestyles.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
</head>
<body>

	<div class="main-registration-container1">
        <div class="register">

          <div class="row">
            <div class="col-sm-3"></div>
            <div class="col-sm-8">
              <h3>New Registration</h3>
            </div>
          </div>
          <form action="candidate/register" method="post" >

            <div class="row">
              <div class="col-sm-1"></div>
              <div class="col-sm-8">
                <label>First Name</label>
                <input type="text" name="firstName"  placeholder="First name" />
              </div>
              <div class="col-sm-2"></div>

            </div>
            <div class="row">
              <div class="col-sm-1"></div>
              <div class="col-sm-8">
                <label>Last Name</label>
                <input type="text" name="lastName"  placeholder="Last name" />
              </div>
              <div class="col-sm-2"></div>

            </div>
            <div class="row">
              <div class="col-sm-1"></div>
              <div class="col-sm-8">
                <label>Email</label>
                <input type="email" name="email" placeholder="ex: m123@gmail.com" />
              </div>
              <div class="col-sm-2"></div>

            </div>

            <div class="row">
              <div class="col-sm-1"></div>
              <div class="col-sm-8">
                <label>Password</label>
                <input type="password" name="password"  placeholder="password" />
              </div>
              <div class="col-sm-2"></div>

            </div>

            <div class="row">
              <div class="col-sm-1"></div>
              <div class="col-sm-8">
                <label>Date of Birth</label>
                <input type="date" name="dateOfBirth"  placeholder="date of birth" />
              </div>
              <div class="col-sm-2"></div>

            </div>

            <div class="row">
              <div class="col-sm-1"></div>
              <div class="col-sm-8">
                <label>Gender:</label>
                <div>
                  <input type="radio" name="gender" />Male

                  <input type="radio" name="gender" />Female

              </div>
              </div>
              <div class="col-sm-2"></div>

            </div>

            <div class="row">
              <div class="col-sm-1"></div>
              <div class="col-sm-8">
                <label>Skills</label>
                <input type="text" name="skillTags" placeholder="Technical skills" />

              </div>
              <div class="col-sm-2"></div>

            </div>

            <div class="row">
              <div class="col-sm-1"></div>
              <div class="col-sm-8">
                <label>Languages</label>
                <input type="text" type="text" name="languageTags"  placeholder="Languages known" />
              </div>
              <div class="col-sm-2"></div>

            </div>

            <div class="row">
              <div class="col-sm-1"></div>
              <div class="col-sm-8">
                <label>Phone Number</label>
                <input type="text" name="phoneNo"   placeholder="Mobile Number" />
              </div>
              <div class="col-sm-2"></div>

            </div>

           <!--  <div class="row">
              <div class="col-sm-1"></div>
              <div class="col-sm-8">
                <label>Upload Image</label>
                <input type="file" name="image" placeholder="ex:example.jpg" />
                <button type="button">upload</button>
              </div>
              <div class="col-sm-2"></div>

            </div>  -->

            <div class="row">
              <div class="col-sm-1"></div>
              <div class="col-sm-7">
                <input type="submit" class="button btn btn-info btn-lg" value = "Register"> </input>        
                </form>
</body>
</html>